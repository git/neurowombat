/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cmath>
#include <cstdio>
#include <typeinfo>
#include <vector>


#include "api/api.hpp"
#include "components/abstract/AbstractConnectors.hpp"
#include "components/abstract/AbstractWeights.hpp"
#include "components/analog/AnalogCapacitors.hpp"
#include "components/analog/AnalogComparators.hpp"
#include "components/analog/AnalogResistors.hpp"
#include "components/analog/AnalogWires.hpp"
#include "components/digital/DigitalConnectors.hpp"
#include "components/digital/MemoryModule.hpp"
#include "engine/SimulationEngine.hpp"
#include "kernel/Kernel.hpp"
#include "math/ActivationFunction.hpp"
#include "math/Distribution.hpp"
#include "math/OdeSystemSolver.hpp"
#include "math/ProcessingUnit.hpp"
#include "neurons/abstract/AbstractNeuron.hpp"
#include "neurons/analog/AnalogNeuron.hpp"
#include "neurons/digital/DigitalNeuron.hpp"


// It is better for API functions to use this pointer instead of
// Kernel::instance() and Kernel::freeInstance() methods;
extern Kernel * kernel;


namespace {


const char * _noObjectWithIdFoundError = "no %s found with id %d in '%s'";
const char * _indexOutOfRangeError = "index out of range in '%s'";
const char * _sparseArrayIsNotAllowedError = "sparse array is not allowed in '%s'";
const char * _unexpectedNmbOfArgsError1 = "unexpected number of arguments for '%s' (%d expected, got %d)";
const char * _unexpectedNmbOfArgsError2 = "unexpected number of arguments for '%s' (%d or %d expected, got %d)";
const char * _unexpectedNmbOfArgsErrorRange = "unexpected number of arguments for '%s' (%d to %d expected, got %d)";
const char * _unexpectedNmbOfArgsErrorActFunc = "unexpected number of arguments for '%s' (%d expected in case of %s activation function, got %d)";
const char * _unexpectedNmbOfArgsErrorProcUnit = "unexpected number of arguments for '%s' (%d expected in case of %s processing unit, got %d)";
const char * _unexpectedNmbOfArgsErrorDistr = "unexpected number of arguments for '%s' (%d expected in case of %s probability distribution, got %d)";
const char * _unexpectedNmbOfInputConnectorsError = "unexpected number of input connectors in '%s' (%d expected, got %d)";
const char * _unexpectedNmbOfInputWiresError = "unexpected number of input wires in '%s' (%d expected, got %d)";
const char * _unexpectedNmbOfWeightsError = "unexpected number of weights in '%s' (%d expected, got %d)";
const char * _unexpectedLengthOfTargetError = "unexpected length of target in '%s' (%d expected, got %d)";
const char * _unexpectedValueOfBetaError = "unexpected value of beta in '%s' (0.95, 0.99 or 0.999 expected, got %f)";
const char * _unexpectedValueOfAlphaError = "unexpected value of alpha in '%s' (0.05, 0.01 or 0.001 expected, got %f)";
const char * _invalidNeuronDistributionError = "invalid neuron distribution per layer in '%s'";
const char * _invalidNeuronsTypeError = "invalid neurons type in '%s'";
const char * _meansqrCantBeNegativeError = "meansqr can't be negative in '%s'";
const char * _meansqrMean2ConditionNotSatisfiedError = "meansqr >= mean * mean condition not satisfied in '%s'";
const char * _timesConditionNotSatisfiedError = "times > 1 condition not satisfied in '%s'";
const char * _pConditionNotSatisfiedError = "0.0 <= p <= 1.0 condition not satisfied in '%s'";


const char * _curFnName(lua_State * L)
{
   lua_Debug ar;
   if (lua_getstack(L, 0, & ar))
   {
      lua_getinfo(L, "n", & ar);
      if (ar.name)
      {
         return ar.name;
      }
   }

   return "?";
}


template <typename T>
T _readVectorConvert(lua_State * L, int index);


template <>
unsigned int _readVectorConvert(lua_State * L, int index)
{
   return lua_tointeger(L, index);
}


template <>
double _readVectorConvert(lua_State * L, int index)
{
   return lua_tonumber(L, index);
}


template <typename T>
void _readVector(lua_State * L, int index, std::vector<T> & vec)
{
   luaL_checktype(L, index, LUA_TTABLE);

   #if LUA_VERSION_NUM >= 502
   const size_t size = lua_rawlen(L, index);
   #else
   const size_t size = lua_objlen(L, index);
   #endif
   size_t count = 0;
   vec.clear();
   vec.resize(size);

   // Push first key;
   lua_pushnil(L);
   while (lua_next(L, index) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      const size_t key = lua_tointeger(L, -2) - 1;
      if (key < size)
      {
         vec[key] = _readVectorConvert<T>(L, -1);
         count ++;
      }
      else
      {
         vec.clear();
         luaL_error(L, _indexOutOfRangeError, _curFnName(L));
         return;
      }

      // Remove 'value' but keep 'key' for next iteration;
      lua_pop(L, 1);
   }

   if (count != size)
   {
      vec.clear();
      luaL_error(L, _sparseArrayIsNotAllowedError, _curFnName(L));
   }
}


bool _isOutOfRange(const std::vector<unsigned int> & vector, unsigned int limit)
{
   for (size_t i = 0; i < vector.size(); ++i)
   {
      if (vector[i] >= limit) return true;
   }

   return false;
}


template <typename T>
void _readKernelObjectsVector(lua_State * L, int index, std::vector<T *> & vec, const char * nameOfType)
{
   luaL_checktype(L, index, LUA_TTABLE);

   #if LUA_VERSION_NUM >= 502
   const size_t size = lua_rawlen(L, index);
   #else
   const size_t size = lua_objlen(L, index);
   #endif
   size_t count = 0;
   vec.clear();
   vec.resize(size, NULL);

   // Push first key;
   lua_pushnil(L);
   while (lua_next(L, index) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      const size_t key = lua_tointeger(L, -2) - 1;
      if (key < size)
      {
         KernelObjectId objectId = lua_tointeger(L, -1);
         T * object = dynamic_cast<T *>(kernel->getObject(objectId));
         if (!object)
         {
            vec.clear();
            luaL_error(L, _noObjectWithIdFoundError, nameOfType, objectId, _curFnName(L));
            return;
         }

         vec[key] = object;
         count ++;
      }
      else
      {
         vec.clear();
         luaL_error(L, _indexOutOfRangeError, _curFnName(L));
         return;
      }

      // Remove 'value' but keep 'key' for next iteration;
      lua_pop(L, 1);
   }

   if (count != size)
   {
      vec.clear();
      luaL_error(L, _sparseArrayIsNotAllowedError, _curFnName(L));
   }
}


/***************************************************************************
 *   Commonn API functions implementation                                  *
 ***************************************************************************/


int _closeId(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   kernel->deleteObject(luaL_checknumber(L, 1));
   return 0;
}


/***************************************************************************
 *   Abstract neuron API functions implementation                          *
 ***************************************************************************/


int _createAbstractConnectors(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      AbstractConnectors * connectors = new AbstractConnectors(count);
      id = kernel->insertObject(connectors);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _getSignals(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read connectors argument;
   KernelObjectId connectorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(connectorsId);
   AbstractConnectors * connectors = dynamic_cast<AbstractConnectors *>(object);
   if (!connectors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AbstractConnectors", connectorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read count argument;
   unsigned int count = luaL_checkinteger(L, 3);

   if (count && (baseIndex + count > connectors->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Create table;
   lua_newtable(L);
   for (unsigned int i = 0; i < count; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, connectors->at(baseIndex + i));
      lua_rawset(L, -3);
   }

   return 1;
}


int _setSignals(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read connectors argument;
   KernelObjectId connectorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(connectorsId);
   AbstractConnectors * connectors = dynamic_cast<AbstractConnectors *>(object);
   if (!connectors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AbstractConnectors", connectorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read signals argument;
   unsigned int limit = connectors->count();
   lua_pushnil(L);
   while (lua_next(L, 3) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
      if (index < limit) connectors->at(index) = lua_tonumber(L, -1);
      else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      lua_pop(L, 1);
   }

   return 0;
}


int _createAbstractWeights(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      AbstractWeights * weights = new AbstractWeights(count);
      id = kernel->insertObject(weights);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _getAbstractWeights(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (!(argc == 1 || argc == 3))
   {
      return luaL_error(L, _unexpectedNmbOfArgsError2, _curFnName(L), 1, 3, argc);
   }

   // Read neuron argument;
   KernelObjectId objectId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(objectId);

   if (argc == 1)
   {
      AbstractNeuron * neuron = dynamic_cast<AbstractNeuron *>(object);
      if (!neuron)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "AbstractNeuron", objectId, _curFnName(L));
      }

      // Create table;
      lua_newtable(L);
      for (unsigned int i = 0; i < neuron->getInputsCount(); ++i)
      {
         // Increase key by 1 to provide compatibility between C and Lua-style arrays;
         lua_pushnumber(L, i + 1);
         lua_pushnumber(L, neuron->getWeight(i));
         lua_rawset(L, -3);
      }
   }
   else
   {
      AbstractWeights * weights = dynamic_cast<AbstractWeights *>(object);
      if (!weights)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "AbstractWeights", objectId, _curFnName(L));
      }

      // Read baseIndex argument;
      unsigned int baseIndex = luaL_checkinteger(L, 2);

      // Read count argument;
      unsigned int count = luaL_checkinteger(L, 3);

      if (count && (baseIndex + count > weights->count()))
      {
         return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      }

      // Create table;
      lua_newtable(L);
      for (unsigned int i = 0; i < count; ++i)
      {
         // Increase key by 1 to provide compatibility between C and Lua-style arrays;
         lua_pushnumber(L, i + 1);
         lua_pushnumber(L, weights->at(baseIndex + i));
         lua_rawset(L, -3);
      }
   }

   return 1;
}


int _setAbstractWeights(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (!(argc == 2 || argc == 3))
   {
      return luaL_error(L, _unexpectedNmbOfArgsError2, _curFnName(L), 2, 3, argc);
   }

   // Read neuron argument;
   KernelObjectId objectId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(objectId);

   if (argc == 2)
   {
      AbstractNeuron * neuron = dynamic_cast<AbstractNeuron *>(object);
      if (!neuron)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "AbstractNeuron", objectId, _curFnName(L));
      }

      // Read weights argument;
      unsigned int limit = neuron->getInputsCount();
      lua_pushnil(L);
      while (lua_next(L, 2) != 0)
      {
         // Read key and decrease it by 1 to provide compatibility
         // between C and Lua-style arrays;
         unsigned int index = lua_tointeger(L, -2) - 1;
         if (index < limit) neuron->setWeight(index, lua_tonumber(L, -1));
         else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
         lua_pop(L, 1);
      }
   }
   else
   {
      AbstractWeights * weights = dynamic_cast<AbstractWeights *>(object);
      if (!weights)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "AbstractWeights", objectId, _curFnName(L));
      }

      // Read baseIndex argument;
      unsigned int baseIndex = luaL_checkinteger(L, 2);

      // Read weights argument;
      unsigned int limit = weights->count();
      lua_pushnil(L);
      while (lua_next(L, 3) != 0)
      {
         // Read key and decrease it by 1 to provide compatibility
         // between C and Lua-style arrays;
         unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
         if (index < limit) weights->at(index) = lua_tonumber(L, -1);
         else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
         lua_pop(L, 1);
      }
   }

   return 0;
}


int _createAbstractNeuron(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 8)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 8, argc);
   }

   KernelObject * object = NULL;

   // Read inputsCount argument;
   unsigned int inputsCount = luaL_checkinteger(L, 1);

   // Read inputConnectors argument;
   std::vector<unsigned int> inputConnectors;
   _readVector(L, 2, inputConnectors);
   if (inputConnectors.size() != inputsCount)
   {
      return luaL_error(L, _unexpectedNmbOfInputConnectorsError, _curFnName(L), inputsCount, inputConnectors.size());
   }

   // Read connectors argument;
   KernelObjectId connectorsId = luaL_checkinteger(L, 3);
   object = kernel->getObject(connectorsId);
   AbstractConnectors * connectors = dynamic_cast<AbstractConnectors *>(object);
   if (!connectors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AbstractConnectors", connectorsId, _curFnName(L));
   }

   // Read connectorsBaseIndex argument;
   unsigned int connectorsBaseIndex = luaL_checkinteger(L, 4);

   // Check connectors;
   const unsigned int connectorsCount = connectors->count();
   if ((connectorsBaseIndex >= connectorsCount) ||
      _isOutOfRange(inputConnectors, connectorsCount))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Read weights argument;
   KernelObjectId weightsId = luaL_checkinteger(L, 5);
   object = kernel->getObject(weightsId);
   AbstractWeights * weights = dynamic_cast<AbstractWeights *>(object);

   // Read weightsBaseIndex argument;
   unsigned int weightsBaseIndex = luaL_checkinteger(L, 6);

   if (weights && inputsCount && (weightsBaseIndex + inputsCount > weights->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Read processingUnit argument;
   KernelObjectId processingUnitId = luaL_checkinteger(L, 7);
   object = kernel->getObject(processingUnitId);
   ProcessingUnit * processingUnit = dynamic_cast<ProcessingUnit *>(object);
   if (!processingUnit)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "ProcessingUnit", processingUnitId, _curFnName(L));
   }

   // Read activationFunction argument;
   KernelObjectId activationFunctionId = luaL_checkinteger(L, 8);
   object = kernel->getObject(activationFunctionId);
   ActivationFunction * activationFunction = dynamic_cast<ActivationFunction *>(object);
   if (!activationFunction)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "ActivationFunction", activationFunctionId, _curFnName(L));
   }

   // Create abstract neuron;
   KernelObjectId id = 0;
   if (!inputConnectors.empty())
   {
      AbstractNeuron * neuron = new AbstractNeuron(
         inputConnectors,
         connectors, connectorsBaseIndex,
         weights, weightsBaseIndex,
         processingUnit,
         activationFunction
      );

      id = kernel->insertObject(neuron);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _computeAbstractNeurons(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 2)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 2, argc);
   }

   // Create vector for holding AbstractNeuron pointers;
   std::vector<AbstractNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "AbstractNeuron");

   // Read times argument;
   unsigned int times = luaL_checkinteger(L, 2);

   // Calculate neurons;
   for (unsigned int i = 0; i < times; ++i)
   {
      for (unsigned int j = 0; j < neurons.size(); ++j)
      {
         if (neurons[j]) neurons[j]->compute();
      }
   }

   return 0;
}


int _computeAbstractNeuronsC(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 4)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 4, argc);
   }

   // Create vector for holding AbstractNeuron pointers;
   std::vector<AbstractNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "AbstractNeuron");

   // Read timeConstant argument;
   double timeConstant = luaL_checkinteger(L, 2);

   // Read time argument;
   double time = luaL_checkinteger(L, 3);

   // Read stepsCount argument;
   unsigned int stepsCount = luaL_checkinteger(L, 4);

   // Solve AbstractNeuronsOdeSystem;
   AbstractNeuronsOdeSystem odeSystem(& neurons, timeConstant);
   OdeSystemSolver::solve(odeSystem, 0, time, stepsCount);

   return 0;
}


int _trainBPAbstractNeurons(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 5)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 5, argc);
   }

   // Create vector for holding AbstractNeuron pointers;
   std::vector<AbstractNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "AbstractNeuron");

   // Create vector for holding neurons per layer count;
   std::vector<unsigned int> layers;

   // Read layers argument;
   _readVector(L, 2, layers);

   // Check layers;
   unsigned int layersSum = 0;
   for (unsigned int i = 0; i < layers.size(); ++i)
   {
      layersSum += layers[i];
   }

   if (layersSum != neurons.size())
   {
      return luaL_error(L, _invalidNeuronDistributionError, _curFnName(L));
   }

   // Read target argument;
   unsigned int targetLength = layers[layers.size() - 1];
   std::vector<double> target;
   _readVector(L, 3, target);
   if (target.size() != targetLength)
   {
      return luaL_error(L, _unexpectedLengthOfTargetError, _curFnName(L), targetLength, target.size());
   }

   // Read damping argument;
   double damping = luaL_checknumber(L, 4);

   // Read speed argument;
   double speed = luaL_checknumber(L, 5);

   // Calculate neurons;
   unsigned int neuronsCount = neurons.size();
   for (unsigned int i = 0; i < neuronsCount; ++i)
   {
      neurons[i]->compute();
   }

   // Snap deltas for output layer;
   for (unsigned int i = 0; i < targetLength; ++i)
   {
      neurons[neuronsCount - 1 - i]->snapDelta(
         target[i] - neurons[neuronsCount - 1 - i]->getOutput()
      );
   }

   // Clear target;
   target.clear();

   // Snap deltas for the rest layers;
   unsigned int index = neuronsCount - targetLength - 1;
   unsigned int prevIndex = neuronsCount - 1;
   for (int i = layers.size() - 2; i >= 0 ; --i)
   {
      for (int j = layers[i] - 1; j >= 0; --j)
      {
         // Calculate error;
         double err = 0.0;
         for (int k = layers[i + 1] - 1; k >= 0; --k)
         {
            err += neurons[prevIndex - k]->getWeightedDelta(j);
         }

         neurons[index]->snapDelta(err);
         index --;
      }

      prevIndex -= layers[i + 1];
   }

   // Modify weights for all the neurons;
   for (unsigned int i = 0; i < neuronsCount; ++i)
   {
      neurons[i]->createDampingBuffers();
      neurons[i]->modifyWeights(damping, speed);
   }

   lua_pushnumber(L, 0);
   return 1;
}


/***************************************************************************
 *   Analog neuron API functions implementation                            *
 ***************************************************************************/


int _createAnalogCapacitors(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      AnalogCapacitors * capacitors = new AnalogCapacitors(count);
      id = kernel->insertObject(capacitors);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _getAnalogCapacitances(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read capacitors argument;
   KernelObjectId capacitorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(capacitorsId);
   AnalogCapacitors * capacitors = dynamic_cast<AnalogCapacitors *>(object);
   if (!capacitors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogCapacitors", capacitorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read count argument;
   unsigned int count = luaL_checkinteger(L, 3);

   if (count && (baseIndex + count > capacitors->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Create table;
   lua_newtable(L);
   for (unsigned int i = 0; i < count; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, capacitors->at(baseIndex + i));
      lua_rawset(L, -3);
   }

   return 1;
}


int _setAnalogCapacitances(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read capacitors argument;
   KernelObjectId capacitorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(capacitorsId);
   AnalogCapacitors * capacitors = dynamic_cast<AnalogCapacitors *>(object);
   if (!capacitors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogCapacitors", capacitorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read capacitances argument;
   unsigned int limit = capacitors->count();
   lua_pushnil(L);
   while (lua_next(L, 3) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
      if (index < limit) capacitors->at(index) = lua_tonumber(L, -1);
      else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      lua_pop(L, 1);
   }

   return 0;
}


int _createAnalogComparators(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      AnalogComparators * comparators = new AnalogComparators(count);
      id = kernel->insertObject(comparators);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _createAnalogResistors(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      AnalogResistors * resistors = new AnalogResistors(count);
      id = kernel->insertObject(resistors);
   }

   lua_pushnumber(L, id);
   return 1;
}


// Count argument must be deleted;
int _setupAnalogResistors(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 5)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 5, argc);
   }

   // Read resistors argument;
   KernelObjectId resistorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(resistorsId);
   AnalogResistors * resistors = dynamic_cast<AnalogResistors *>(object);
   if (!resistors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogResistors", resistorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read count argument;
   unsigned int count = luaL_checkinteger(L, 3);

   // Read weights argument;
   std::vector<double> weights;
   _readVector(L, 4, weights);
   if (weights.size() != count)
   {
      return luaL_error(L, _unexpectedNmbOfWeightsError, _curFnName(L), count, weights.size());
   }

   // Read numCopies argument;
   unsigned int numCopies = luaL_checkinteger(L, 5);

   if (count && numCopies && (baseIndex + (count * numCopies) > resistors->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Calculate weights product;
   double wProduct = 1.0;
   unsigned int numNonZeroW = 0;
   for (unsigned int j = 0; j < count; ++j)
   {
      if (weights[j] != 0.0)
      {
         numNonZeroW ++;
         wProduct *= weights[j];
      }
   }

   // Calculate resistances;
   for (unsigned int j = 0; j < count; ++j)
   {
      double resistance = 0.0;
      if (weights[j] != 0.0)
      {
         if (numNonZeroW > 1)
         {
            resistance = pow(fabs(wProduct), 1.0 / (double) (numNonZeroW - 1)) / weights[j];
         }
         else
         {
            resistance = weights[j];
         }
      }

      // Set resistances;
      for (unsigned int k = 0; k < numCopies; ++k)
      {
         resistors->at(baseIndex + count * k + j) = resistance;
      }
   }

   return 0;
}


int _getAnalogResistances(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read resistors argument;
   KernelObjectId resistorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(resistorsId);
   AnalogResistors * resistors = dynamic_cast<AnalogResistors *>(object);
   if (!resistors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogResistors", resistorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read count argument;
   unsigned int count = luaL_checkinteger(L, 3);

   if (count && (baseIndex + count > resistors->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Create table;
   lua_newtable(L);
   for (unsigned int i = 0; i < count; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, resistors->at(baseIndex + i));
      lua_rawset(L, -3);
   }

   return 1;
}


int _setAnalogResistances(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read resistors argument;
   KernelObjectId resistorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(resistorsId);
   AnalogResistors * resistors = dynamic_cast<AnalogResistors *>(object);
   if (!resistors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogResistors", resistorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read resistances argument;
   unsigned int limit = resistors->count();
   lua_pushnil(L);
   while (lua_next(L, 3) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
      if (index < limit) resistors->at(index) = lua_tonumber(L, -1);
      else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      lua_pop(L, 1);
   }

   return 0;
}


int _createAnalogWires(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      AnalogWires * wires = new AnalogWires(count);
      id = kernel->insertObject(wires);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _getPotentials(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read wires argument;
   KernelObjectId wiresId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(wiresId);
   AnalogWires * wires = dynamic_cast<AnalogWires *>(object);
   if (!wires)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogWires", wiresId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read count argument;
   unsigned int count = luaL_checkinteger(L, 3);

   if (count && (baseIndex + count > wires->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Create table;
   lua_newtable(L);
   for (unsigned int i = 0; i < count; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, wires->at(baseIndex + i));
      lua_rawset(L, -3);
   }

   return 1;
}


int _setPotentials(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read wires argument;
   KernelObjectId wiresId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(wiresId);
   AnalogWires * wires = dynamic_cast<AnalogWires *>(object);
   if (!wires)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogWires", wiresId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read potentials argument;
   unsigned int limit = wires->count();
   lua_pushnil(L);
   while (lua_next(L, 3) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
      if (index < limit) wires->at(index) = lua_tonumber(L, -1);
      else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      lua_pop(L, 1);
   }

   return 0;
}


int _createAnalogNeuron(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 12)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 12, argc);
   }

   KernelObject * object = NULL;

   // Read inputsCount argument;
   unsigned int inputsCount = luaL_checkinteger(L, 1);

   // Read inputWires argument;
   std::vector<unsigned int> inputWires;
   _readVector(L, 2, inputWires);
   if (inputWires.size() != inputsCount)
   {
      return luaL_error(L, _unexpectedNmbOfInputWiresError, _curFnName(L), inputsCount, inputWires.size());
   }

   // Read gndWireIndex argument;
   unsigned int gndWireIndex = luaL_checkinteger(L, 3);

   // Read srcWireIndex argument;
   unsigned int srcWireIndex = luaL_checkinteger(L, 4);

   // Read capacitors argument;
   KernelObjectId capacitorsId = luaL_checkinteger(L, 5);
   object = kernel->getObject(capacitorsId);
   AnalogCapacitors * capacitors = dynamic_cast<AnalogCapacitors *>(object);
   if (!capacitors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogCapacitors", capacitorsId, _curFnName(L));
   }

   // Read capacitorsBaseIndex argument;
   unsigned int capacitorsBaseIndex = luaL_checkinteger(L, 6);

   // Read comparators argument;
   KernelObjectId comparatorsId = luaL_checkinteger(L, 7);
   object = kernel->getObject(comparatorsId);
   AnalogComparators * comparators = dynamic_cast<AnalogComparators *>(object);

   // Read comparatorsBaseIndex argument;
   unsigned int comparatorsBaseIndex = luaL_checkinteger(L, 8);

   if (comparators && (comparatorsBaseIndex >= comparators->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   if (capacitorsBaseIndex + (comparators ? 1 : 0) >= capacitors->count())
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Read resistors argument;
   KernelObjectId resistorsId = luaL_checkinteger(L, 9);
   object = kernel->getObject(resistorsId);
   AnalogResistors * resistors = dynamic_cast<AnalogResistors *>(object);
   if (!resistors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogResistors", resistorsId, _curFnName(L));
   }

   // Read resistorsBaseIndex argument;
   unsigned int resistorsBaseIndex = luaL_checkinteger(L, 10);

   const unsigned int resistorsNeeded = (comparators ? (inputsCount * 2) : inputsCount);
   if (inputsCount && (resistorsBaseIndex + resistorsNeeded > resistors->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Read wires argument;
   KernelObjectId wiresId = luaL_checkinteger(L, 11);
   object = kernel->getObject(wiresId);
   AnalogWires * wires = dynamic_cast<AnalogWires *>(object);
   if (!wires)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "AnalogWires", wiresId, _curFnName(L));
   }

   // Read wiresBaseIndex argument;
   unsigned int wiresBaseIndex = luaL_checkinteger(L, 12);

   // Check wires;
   const unsigned int wiresCount = wires->count();
   if ((wiresBaseIndex >= wiresCount) ||
      (gndWireIndex >= wiresCount) ||
      (srcWireIndex >= wiresCount) ||
      _isOutOfRange(inputWires, wiresCount))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Create analog neuron;
   KernelObjectId id = 0;
   if (!inputWires.empty())
   {
      AnalogNeuron * neuron = new AnalogNeuron(
         inputWires,
         gndWireIndex,
         srcWireIndex,
         capacitors,
         capacitorsBaseIndex,
         comparators,
         comparatorsBaseIndex,
         resistors,
         resistorsBaseIndex,
         wires,
         wiresBaseIndex
      );

      id = kernel->insertObject(neuron);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _computeAnalogNeurons(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 2)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 2, argc);
   }

   // Create vector for holding AnalogNeuron pointers;
   std::vector<AnalogNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "AnalogNeuron");

   // Read times argument;
   unsigned int times = luaL_checkinteger(L, 2);

   // Calculate neurons;
   for (unsigned int i = 0; i < times; ++i)
   {
      for (unsigned int j = 0; j < neurons.size(); ++j)
      {
         if (neurons[j]) neurons[j]->compute();
      }
   }

   return 0;
}


int _computeAnalogLimNeuronsC(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Create vector for holding AnalogNeuron pointers;
   std::vector<AnalogNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "AnalogNeuron");

   // Check neurons;
   for (unsigned int i = 0; i < neurons.size(); ++i)
   {
      if (!neurons[i]->comparators())
      {
         return luaL_error(L, _invalidNeuronsTypeError, _curFnName(L));
      }
   }

   // Read time argument;
   double time = luaL_checkinteger(L, 2);

   // Read stepsCount argument;
   unsigned int stepsCount = luaL_checkinteger(L, 3);

   // Solve AnalogLimNeuronsOdeSystem;
   AnalogLimNeuronsOdeSystem odeSystem(& neurons);
   OdeSystemSolver::solve(odeSystem, 0, time, stepsCount);

   return 0;
}


/***************************************************************************
 *   Digital neuron API functions implementation                           *
 ***************************************************************************/


int _createDigitalConnectors(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      DigitalConnectors * connectors = new DigitalConnectors(count);
      id = kernel->insertObject(connectors);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _getValues(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read connectors argument;
   KernelObjectId connectorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(connectorsId);
   DigitalConnectors * connectors = dynamic_cast<DigitalConnectors *>(object);
   if (!connectors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "DigitalConnectors", connectorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read count argument;
   unsigned int count = luaL_checkinteger(L, 3);

   if (count && (baseIndex + count > connectors->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Create table;
   lua_newtable(L);
   for (unsigned int i = 0; i < count; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, connectors->at(baseIndex + i));
      lua_rawset(L, -3);
   }

   return 1;
}


int _setValues(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read connectors argument;
   KernelObjectId connectorsId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(connectorsId);
   DigitalConnectors * connectors = dynamic_cast<DigitalConnectors *>(object);
   if (!connectors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "DigitalConnectors", connectorsId, _curFnName(L));
   }

   // Read baseIndex argument;
   unsigned int baseIndex = luaL_checkinteger(L, 2);

   // Read signals argument;
   unsigned int limit = connectors->count();
   lua_pushnil(L);
   while (lua_next(L, 3) != 0)
   {
      // Read key and decrease it by 1 to provide compatibility
      // between C and Lua-style arrays;
      unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
      if (index < limit) connectors->at(index) = lua_tonumber(L, -1);
      else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      lua_pop(L, 1);
   }

   return 0;
}


int _createMemoryModule(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   KernelObjectId id = 0;

   unsigned int count = luaL_checkinteger(L, 1);
   if (count > 0)
   {
      MemoryModule * memory = new MemoryModule(count);
      id = kernel->insertObject(memory);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _getDigitalWeights(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (!(argc == 1 || argc == 3))
   {
      return luaL_error(L, _unexpectedNmbOfArgsError2, _curFnName(L), 1, 3, argc);
   }

   // Read neuron argument;
   KernelObjectId objectId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(objectId);

   if (argc == 1)
   {
      DigitalNeuron * neuron = dynamic_cast<DigitalNeuron *>(object);
      if (!neuron)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "DigitalNeuron", objectId, _curFnName(L));
      }

      // Create table;
      lua_newtable(L);
      for (unsigned int i = 0; i < neuron->getInputsCount(); ++i)
      {
         // Increase key by 1 to provide compatibility between C and Lua-style arrays;
         lua_pushnumber(L, i + 1);
         lua_pushnumber(L, neuron->getWeight(i));
         lua_rawset(L, -3);
      }
   }
   else
   {
      MemoryModule * memory = dynamic_cast<MemoryModule *>(object);
      if (!memory)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "MemoryModule", objectId, _curFnName(L));
      }

      // Read baseIndex argument;
      unsigned int baseIndex = luaL_checkinteger(L, 2);

      // Read count argument;
      unsigned int count = luaL_checkinteger(L, 3);

      if (count && (baseIndex + count > memory->count()))
      {
         return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
      }

      // Create table;
      lua_newtable(L);
      for (unsigned int i = 0; i < count; ++i)
      {
         // Increase key by 1 to provide compatibility between C and Lua-style arrays;
         lua_pushnumber(L, i + 1);
         lua_pushnumber(L, memory->at(baseIndex + i));
         lua_rawset(L, -3);
      }
   }

   return 1;
}


int _setDigitalWeights(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (!(argc == 2 || argc == 3))
   {
      return luaL_error(L, _unexpectedNmbOfArgsError2, _curFnName(L), 2, 3, argc);
   }

   // Read neuron argument;
   KernelObjectId objectId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(objectId);

   if (argc == 2)
   {
      DigitalNeuron * neuron = dynamic_cast<DigitalNeuron *>(object);
      if (!neuron)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "DigitalNeuron", objectId, _curFnName(L));
      }

      // Read weights argument;
      unsigned int limit = neuron->getInputsCount();
      lua_pushnil(L);
      while (lua_next(L, 2) != 0)
      {
         // Read key and decrease it by 1 to provide compatibility
         // between C and Lua-style arrays;
         unsigned int index = lua_tointeger(L, -2) - 1;
         if (index < limit) neuron->setWeight(index, lua_tonumber(L, -1));
         else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
         lua_pop(L, 1);
      }
   }
   else
   {
      MemoryModule * memory = dynamic_cast<MemoryModule *>(object);
      if (!memory)
      {
         return luaL_error(L, _noObjectWithIdFoundError, "MemoryModule", objectId, _curFnName(L));
      }

      // Read baseIndex argument;
      unsigned int baseIndex = luaL_checkinteger(L, 2);

      // Read weights argument;
      unsigned int limit = memory->count();
      lua_pushnil(L);
      while (lua_next(L, 3) != 0)
      {
         // Read key and decrease it by 1 to provide compatibility
         // between C and Lua-style arrays;
         unsigned int index = baseIndex + lua_tointeger(L, -2) - 1;
         if (index < limit) memory->at(index) = lua_tonumber(L, -1);
         else return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
         lua_pop(L, 1);
      }
   }

   return 0;
}


int _createDigitalNeuron(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 8)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 8, argc);
   }

   KernelObject * object = NULL;

   // Read inputsCount argument;
   unsigned int inputsCount = luaL_checkinteger(L, 1);

   // Read inputConnectors argument;
   std::vector<unsigned int> inputConnectors;
   _readVector(L, 2, inputConnectors);
   if (inputConnectors.size() != inputsCount)
   {
      return luaL_error(L, _unexpectedNmbOfInputConnectorsError, _curFnName(L), inputsCount, inputConnectors.size());
   }

   // Read connectors argument;
   KernelObjectId connectorsId = luaL_checkinteger(L, 3);
   object = kernel->getObject(connectorsId);
   DigitalConnectors * connectors = dynamic_cast<DigitalConnectors *>(object);
   if (!connectors)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "DigitalConnectors", connectorsId, _curFnName(L));
   }

   // Read connectorsBaseIndex argument;
   unsigned int connectorsBaseIndex = luaL_checkinteger(L, 4);

   // Check connectors;
   const unsigned int connectorsCount = connectors->count();
   if ((connectorsBaseIndex >= connectorsCount) ||
      _isOutOfRange(inputConnectors, connectorsCount))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Read memory argument;
   KernelObjectId memoryId = luaL_checkinteger(L, 5);
   object = kernel->getObject(memoryId);
   MemoryModule * memory = dynamic_cast<MemoryModule *>(object);
   if (!memory)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "MemoryModule", memoryId, _curFnName(L));
   }

   // Read memoryBaseIndex argument;
   unsigned int memoryBaseIndex = luaL_checkinteger(L, 6);

   if (inputsCount && (memoryBaseIndex + inputsCount > memory->count()))
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   // Read processingUnit argument;
   KernelObjectId processingUnitId = luaL_checkinteger(L, 7);
   object = kernel->getObject(processingUnitId);
   ProcessingUnit * processingUnit = dynamic_cast<ProcessingUnit *>(object);
   if (!processingUnit)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "ProcessingUnit", processingUnitId, _curFnName(L));
   }

   // Read activationFunction argument;
   KernelObjectId activationFunctionId = luaL_checkinteger(L, 8);
   object = kernel->getObject(activationFunctionId);
   ActivationFunction * activationFunction = dynamic_cast<ActivationFunction *>(object);
   if (!activationFunction)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "ActivationFunction", activationFunctionId, _curFnName(L));
   }

   // Create digital neuron;
   KernelObjectId id = 0;
   if (!inputConnectors.empty())
   {
      DigitalNeuron * neuron = new DigitalNeuron(
         inputConnectors,
         connectors, connectorsBaseIndex,
         memory, memoryBaseIndex,
         processingUnit,
         activationFunction
      );

      id = kernel->insertObject(neuron);
   }

   lua_pushnumber(L, id);
   return 1;
}


int _computeDigitalNeurons(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 2)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 2, argc);
   }

   // Create vector for holding DigitalNeuron pointers;
   std::vector<DigitalNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "DigitalNeuron");

   // Read times argument;
   unsigned int times = luaL_checkinteger(L, 2);

   // Calculate neurons;
   for (unsigned int i = 0; i < times; ++i)
   {
      for (unsigned int j = 0; j < neurons.size(); ++j)
      {
         if (neurons[j]) neurons[j]->compute();
      }
   }

   return 0;
}


int _trainBPDigitalNeurons(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 5)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 5, argc);
   }

   // Create vector for holding DigitalNeuron pointers;
   std::vector<DigitalNeuron *> neurons;

   // Read neurons argument;
   _readKernelObjectsVector(L, 1, neurons, "DigitalNeuron");

   // Create vector for holding neurons per layer count;
   std::vector<unsigned int> layers;

   // Read layers argument;
   _readVector(L, 2, layers);

   // Check layers;
   unsigned int layersSum = 0;
   for (unsigned int i = 0; i < layers.size(); ++i)
   {
      layersSum += layers[i];
   }

   if (layersSum != neurons.size())
   {
      return luaL_error(L, _invalidNeuronDistributionError, _curFnName(L));
   }

   // Read target argument;
   unsigned int targetLength = layers[layers.size() - 1];
   std::vector<double> target;
   _readVector(L, 3, target);
   if (target.size() != targetLength)
   {
      return luaL_error(L, _unexpectedLengthOfTargetError, _curFnName(L), targetLength, target.size());
   }

   // Read damping argument;
   double damping = luaL_checknumber(L, 4);

   // Read speed argument;
   double speed = luaL_checknumber(L, 5);

   // Calculate neurons;
   unsigned int neuronsCount = neurons.size();
   for (unsigned int i = 0; i < neuronsCount; ++i)
   {
      neurons[i]->compute();
   }

   // Snap deltas for output layer;
   for (unsigned int i = 0; i < targetLength; ++i)
   {
      neurons[neuronsCount - 1 - i]->snapDelta(
         target[i] - neurons[neuronsCount - 1 - i]->getOutput()
      );
   }

   // Clear target;
   target.clear();

   // Snap deltas for the rest layers;
   unsigned int index = neuronsCount - targetLength - 1;
   unsigned int prevIndex = neuronsCount - 1;
   for (int i = layers.size() - 2; i >= 0 ; --i)
   {
      for (int j = layers[i] - 1; j >= 0; --j)
      {
         // Calculate error;
         double err = 0.0;
         for (int k = layers[i + 1] - 1; k >= 0; --k)
         {
            err += neurons[prevIndex - k]->getWeightedDelta(j);
         }

         neurons[index]->snapDelta(err);
         index --;
      }

      prevIndex -= layers[i + 1];
   }

   // Modify weights for all the neurons;
   for (unsigned int i = 0; i < neuronsCount; ++i)
   {
      neurons[i]->createDampingBuffers();
      neurons[i]->modifyWeights(damping, speed);
   }

   lua_pushnumber(L, 0);
   return 1;
}


/***************************************************************************
 *   Math API functions implementation                                     *
 ***************************************************************************/


int _createActFunc(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc < 1 || argc > 5)
   {
      return luaL_error(L, _unexpectedNmbOfArgsErrorRange, _curFnName(L), 1, 5, argc);
   }

   KernelObjectId id = 0;

   // Read actFunc argument;
   int actFunc = luaL_checknumber(L, 1);
   switch (actFunc)
   {
      case ACT_FUNC::CUSTOM:
      {
         if (argc != 3)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 3, "CUSTOM", argc);
         }

         // Read luaFunction argument;
         CustomFunction * luaFunction = new CustomFunction(2);

         // Read luaDerivative argument;
         CustomFunction * luaDerivative = new CustomFunction(3);

         id = kernel->insertObject(new CustomActivationFunction(luaFunction, luaDerivative));
         break;
      }
      case ACT_FUNC::GAUSSIAN:
      {
         if (argc != 2)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 2, "GAUSSIAN", argc);
         }

         double beta = luaL_checknumber(L, 2);
         id = kernel->insertObject(new GaussianActivationFunction(beta));
         break;
      }
      case ACT_FUNC::LIM:
      {
         if (argc != 4)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 4, "LIM", argc);
         }

         double xLim = luaL_checknumber(L, 2);
         double yLow = luaL_checknumber(L, 3);
         double yHigh = luaL_checknumber(L, 4);
         id = kernel->insertObject(new LimActivationFunction(xLim, yLow, yHigh));
         break;
      }
      case ACT_FUNC::LINEAR:
      {
         if (argc != 3)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 3, "LINEAR", argc);
         }

         double a = luaL_checknumber(L, 2);
         double b = luaL_checknumber(L, 3);
         id = kernel->insertObject(new LinearActivationFunction(a, b));
         break;
      }
      case ACT_FUNC::LIMLINEAR:
      {
         if (argc != 5)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 5, "LIMLINEAR", argc);
         }

         double a = luaL_checknumber(L, 2);
         double b = luaL_checknumber(L, 3);
         double xMin = luaL_checknumber(L, 4);
         double xMax = luaL_checknumber(L, 5);
         id = kernel->insertObject(new LimLinearActivationFunction(a, b, xMin, xMax));
         break;
      }
      case ACT_FUNC::POSLINEAR:
      {
         if (argc != 3)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 3, "POSLINEAR", argc);
         }

         double a = luaL_checknumber(L, 2);
         double b = luaL_checknumber(L, 3);
         id = kernel->insertObject(new PosLinearActivationFunction(a, b));
         break;
      }
      case ACT_FUNC::SIGMOID:
      {
         if (argc != 2)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 2, "SIGMOID", argc);
         }

         double lambda = luaL_checknumber(L, 2);
         id = kernel->insertObject(new SigmoidActivationFunction(lambda));
         break;
      }
      case ACT_FUNC::THSIGMOID:
      {
         if (argc != 1)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorActFunc, _curFnName(L), 1, "THSIGMOID", argc);
         }

         id = kernel->insertObject(new ThSigmoidActivationFunction());
         break;
      }
      default:
         // Do nothing;
         break;
   }

   lua_pushnumber(L, id);
   return 1;
}


int _createProcUnit(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (!(argc == 1 || argc == 2))
   {
      return luaL_error(L, _unexpectedNmbOfArgsError2, _curFnName(L), 1, 2, argc);
   }

   KernelObjectId id = 0;

   // Read procUnit argument;
   int procUnit = luaL_checknumber(L, 1);
   switch (procUnit)
   {
      case PROC_UNIT::CUSTOM:
      {
         if (argc != 2)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorProcUnit, _curFnName(L), 2, "CUSTOM", argc);
         }

         // Read luaFunction argument;
         CustomFunction * luaFunction = new CustomFunction(2);

         id = kernel->insertObject(new CustomProcessingUnit(luaFunction));
         break;
      }
      case PROC_UNIT::RADIAL_BASIS:
      {
         if (argc != 2)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorProcUnit, _curFnName(L), 2, "RADIAL_BASIS", argc);
         }

         // Read coeffUsage argument;
         COEFF_USAGE::T_COEFF_USAGE coeffUsage = (COEFF_USAGE::T_COEFF_USAGE) luaL_checknumber(L, 2);

         id = kernel->insertObject(new RadialBasisProcessingUnit(coeffUsage));
         break;
      }
      case PROC_UNIT::SCALAR:
      {
         if (argc != 1)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorProcUnit, _curFnName(L), 1, "SCALAR", argc);
         }

         id = kernel->insertObject(new ScalarProcessingUnit());
         break;
      }
      case PROC_UNIT::WEIGHTED_SUM:
      {
         if (argc != 1)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorProcUnit, _curFnName(L), 1, "WEIGHTED_SUM", argc);
         }

         id = kernel->insertObject(new WeightedSumProcessingUnit());
         break;
      }
      default:
         // Do nothing;
         break;
   }

   lua_pushnumber(L, id);
   return 1;
}


int _createDistribution(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc < 1 || argc > 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsErrorRange, _curFnName(L), 1, 3, argc);
   }

   KernelObjectId id = 0;

   // Read distr argument;
   int distr = luaL_checknumber(L, 1);
   switch (distr)
   {
      case DISTR::CUSTOM:
      {
         if (argc != 2)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorDistr, _curFnName(L), 2, "CUSTOM", argc);
         }

         CustomFunction * luaInverseFunction = new CustomFunction(2);
         id = kernel->insertObject(new CustomDistribution(luaInverseFunction));
         break;
      }
      case DISTR::EXP:
      {
         if (argc != 2)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorDistr, _curFnName(L), 2, "EXP", argc);
         }

         double lambda = luaL_checknumber(L, 2);
         id = kernel->insertObject(new ExponentialDistribution(lambda));
         break;
      }
      case DISTR::WEIBULL:
      {
         if (argc != 3)
         {
            return luaL_error(L, _unexpectedNmbOfArgsErrorDistr, _curFnName(L), 3, "WEIBULL", argc);
         }

         double teta = luaL_checknumber(L, 2);
         double beta = luaL_checknumber(L, 3);
         id = kernel->insertObject(new WeibullDistribution(teta, beta));
         break;
      }
      default:
         // Do nothing;
         break;
   }

   lua_pushnumber(L, id);
   return 1;
}


int _calcMeanCI(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 4)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 4, argc);
   }

   // Read mean argument;
   double mean = luaL_checknumber(L, 1);
   double mean2 = mean * mean;

   // Read meansqr argument;
   double meansqr = luaL_checknumber(L, 2);

   if (meansqr < 0.0)
   {
      return luaL_error(L, _meansqrCantBeNegativeError, _curFnName(L));
   }

   if (meansqr < mean2)
   {
      return luaL_error(L, _meansqrMean2ConditionNotSatisfiedError, _curFnName(L));
   }

   // Read times argument;
   unsigned int times = luaL_checkinteger(L, 3);

   if (times <= 1)
   {
      return luaL_error(L, _timesConditionNotSatisfiedError, _curFnName(L));
   }

   // Read beta argument;
   double beta = luaL_checknumber(L, 4);

   double t = 0.0;
   if (fabs(beta - 0.95) < 0.0001) t = 1.960;
   else if (fabs(beta - 0.99) < 0.0001) t = 2.576;
   else if (fabs(beta - 0.999) < 0.0001) t = 3.291;
   else luaL_error(L, _unexpectedValueOfBetaError, _curFnName(L), beta);

   double delta = t * sqrt((meansqr - mean2) / static_cast<double>(times - 1));

   lua_pushnumber(L, delta);
   return 1;
}


int _calcACProbabilityCI(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   // Read p argument;
   double p = luaL_checknumber(L, 1);

   if (p < 0.0 || p > 1.0)
   {
      return luaL_error(L, _pConditionNotSatisfiedError, _curFnName(L));
   }

   // Read times argument;
   unsigned int times = luaL_checkinteger(L, 2);

   // Read alpha argument;
   double alpha = luaL_checknumber(L, 3);

   double x = 0.0;
   if (fabs(alpha - 0.05) < 0.0001) x = 1.959964;
   else if (fabs(alpha - 0.01) < 0.0001) x = 2.5758293;
   else if (fabs(alpha - 0.001) < 0.0001) x = 3.2905267;
   else luaL_error(L, _unexpectedValueOfAlphaError, _curFnName(L), alpha);

   double timesAC = static_cast<double>(times) + x * x;
   double pAC = (p * static_cast<double>(times) + 0.5 * x * x) / timesAC;
   double delta = x * sqrt(pAC * (1.0 - pAC) / timesAC);

   lua_pushnumber(L, pAC - delta);
   lua_pushnumber(L, pAC + delta);
   return 2;
}


/***************************************************************************
 *   Simulation engine API functions implementation                        *
 ***************************************************************************/


int _createInterruptManager(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 3)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 3, argc);
   }

   KernelObjectId id = 0;

   // Read component argument;
   KernelObjectId componentId = luaL_checkinteger(L, 1);
   KernelObject * component = kernel->getObject(componentId);

   // Read distribution argument;
   KernelObjectId distributionId = luaL_checkinteger(L, 2);
   KernelObject * object = kernel->getObject(distributionId);
   Distribution * distribution = dynamic_cast<Distribution *>(object);
   if (!distribution)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "Distribution", distributionId, _curFnName(L));
   }

   // Read fixFunction argument;
   CustomFunction * fixFunction = NULL;
   if (!lua_isnil(L, 3))
   {
      fixFunction = new CustomFunction(3);
   }

   // Read intSrc argument;
   AbstractWeights * weights = dynamic_cast<AbstractWeights *>(component);
   if (weights)
   {
      id = kernel->insertObject(new AbstractWeightsManager(distribution, weights, fixFunction));
      lua_pushnumber(L, id);
      return 1;
   }

   AnalogCapacitors * capacitors = dynamic_cast<AnalogCapacitors *>(component);
   if (capacitors)
   {
      id = kernel->insertObject(new AnalogCapacitorsManager(distribution, capacitors, fixFunction));
      lua_pushnumber(L, id);
      return 1;
   }

   AnalogResistors * resistors = dynamic_cast<AnalogResistors *>(component);
   if (resistors)
   {
      id = kernel->insertObject(new AnalogResistorsManager(distribution, resistors, fixFunction));
      lua_pushnumber(L, id);
      return 1;
   }

   MemoryModule * memory = dynamic_cast<MemoryModule *>(component);
   if (memory)
   {
      id = kernel->insertObject(new MemoryModuleManager(distribution, memory, fixFunction));
      lua_pushnumber(L, id);
      return 1;
   }

   lua_pushnumber(L, id);
   return 1;
}


int _createSimulationEngine(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 0)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 0, argc);
   }

   SimulationEngine * engine = new SimulationEngine();
   KernelObjectId id = kernel->insertObject(engine);

   lua_pushnumber(L, id);
   return 1;
}


int _appendInterruptManager(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 2)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 2, argc);
   }

   KernelObject * object = NULL;

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   // Read manager argument;
   KernelObjectId managerId = luaL_checkinteger(L, 2);
   object = kernel->getObject(managerId);
   InterruptManager * manager = dynamic_cast<InterruptManager *>(object);
   if (!manager)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "InterruptManager", managerId, _curFnName(L));
   }

   // Append manager;
   engine->appendManager(manager);

   return 0;
}


int _getIntSourcesCount(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read manager argument;
   KernelObjectId managerId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(managerId);
   InterruptManager * manager = dynamic_cast<InterruptManager *>(object);
   if (!manager)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "InterruptManager", managerId, _curFnName(L));
   }

   lua_pushnumber(L, manager->intSourcesCount());
   return 1;
}


int _getInterruptsCount(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read manager argument;
   KernelObjectId managerId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(managerId);
   InterruptManager * manager = dynamic_cast<InterruptManager *>(object);
   if (!manager)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "InterruptManager", managerId, _curFnName(L));
   }

   lua_pushnumber(L, manager->interruptsCount());
   return 1;
}


int _simulateInterrupt(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 2)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 2, argc);
   }

   // Read manager argument;
   KernelObjectId managerId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(managerId);
   InterruptManager * manager = dynamic_cast<InterruptManager *>(object);
   if (!manager)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "InterruptManager", managerId, _curFnName(L));
   }

   // Read intSource argument;
   unsigned int intSource = luaL_checkinteger(L, 2);

   if (intSource >= manager->intSourcesCount())
   {
      return luaL_error(L, _indexOutOfRangeError, _curFnName(L));
   }

   manager->simulateInterrupt(intSource);
   return 0;
}


int _restartEngine(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   // Restart engine;
   engine->restart();

   return 0;
}


int _stepOverEngine(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   lua_pushboolean(L, engine->stepOver());
   return 1;
}


int _getCurrentTime(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   lua_pushnumber(L, engine->currentTime());
   return 1;
}


int _getFutureTime(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   lua_pushnumber(L, engine->futureTime());
   return 1;
}


int _getCurrentSource(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   KernelObjectId managerId = 0;
   int intSource = -1;

   InterruptManager * manager = engine->currentIntSource();
   if (manager)
   {
      managerId = kernel->getId(manager);
      intSource = manager->lastIntSource();
   }

   lua_pushnumber(L, managerId);
   lua_pushnumber(L, intSource);
   return 2;
}


int _getFutureSource(lua_State * L)
{
   const int argc = lua_gettop(L);
   if (argc != 1)
   {
      return luaL_error(L, _unexpectedNmbOfArgsError1, _curFnName(L), 1, argc);
   }

   // Read engine argument;
   KernelObjectId engineId = luaL_checkinteger(L, 1);
   KernelObject * object = kernel->getObject(engineId);
   SimulationEngine * engine = dynamic_cast<SimulationEngine *>(object);
   if (!engine)
   {
      return luaL_error(L, _noObjectWithIdFoundError, "SimulationEngine", engineId, _curFnName(L));
   }

   KernelObjectId managerId = 0;
   int intSource = -1;

   InterruptManager * manager = engine->futureIntSource();
   if (manager)
   {
      managerId = kernel->getId(manager);
      intSource = manager->intSource();
   }

   lua_pushnumber(L, managerId);
   lua_pushnumber(L, intSource);
   return 2;
}


} // anonymous namespace;


void registerApiFunctions(lua_State * L)
{
   // Register common API functions;
   lua_register(L, "closeId", _closeId);
   // Register abstract neuron API functions;
   lua_register(L, "createAbstractConnectors", _createAbstractConnectors);
   lua_register(L, "getSignals", _getSignals);
   lua_register(L, "setSignals", _setSignals);
   lua_register(L, "createAbstractWeights", _createAbstractWeights);
   lua_register(L, "getAbstractWeights", _getAbstractWeights);
   lua_register(L, "setAbstractWeights", _setAbstractWeights);
   lua_register(L, "createAbstractNeuron", _createAbstractNeuron);
   lua_register(L, "computeAbstractNeurons", _computeAbstractNeurons);
   lua_register(L, "computeAbstractNeuronsC", _computeAbstractNeuronsC);
   lua_register(L, "trainBPAbstractNeurons", _trainBPAbstractNeurons);
   // Register analog neuron API functions;
   lua_register(L, "createAnalogCapacitors", _createAnalogCapacitors);
   lua_register(L, "getAnalogCapacitances", _getAnalogCapacitances);
   lua_register(L, "setAnalogCapacitances", _setAnalogCapacitances);
   lua_register(L, "createAnalogComparators", _createAnalogComparators);
   lua_register(L, "createAnalogResistors", _createAnalogResistors);
   lua_register(L, "setupAnalogResistors", _setupAnalogResistors);
   lua_register(L, "getAnalogResistances", _getAnalogResistances);
   lua_register(L, "setAnalogResistances", _setAnalogResistances);
   lua_register(L, "createAnalogWires", _createAnalogWires);
   lua_register(L, "getPotentials", _getPotentials);
   lua_register(L, "setPotentials", _setPotentials);
   lua_register(L, "createAnalogNeuron", _createAnalogNeuron);
   lua_register(L, "computeAnalogNeurons", _computeAnalogNeurons);
   lua_register(L, "computeAnalogLimNeuronsC", _computeAnalogLimNeuronsC);
   // Register digital neuron API functions;
   lua_register(L, "createDigitalConnectors", _createDigitalConnectors);
   lua_register(L, "getValues", _getValues);
   lua_register(L, "setValues", _setValues);
   lua_register(L, "createMemoryModule", _createMemoryModule);
   lua_register(L, "getDigitalWeights", _getDigitalWeights);
   lua_register(L, "setDigitalWeights", _setDigitalWeights);
   lua_register(L, "createDigitalNeuron", _createDigitalNeuron);
   lua_register(L, "computeDigitalNeurons", _computeDigitalNeurons);
   lua_register(L, "trainBPDigitalNeurons", _trainBPDigitalNeurons);
   // Math API functions;
   lua_register(L, "createActFunc", _createActFunc);
   lua_register(L, "createProcUnit", _createProcUnit);
   lua_register(L, "createDistribution", _createDistribution);
   lua_register(L, "calcMeanCI", _calcMeanCI);
   lua_register(L, "calcACProbabilityCI", _calcACProbabilityCI);
   // Simulation engine API functions;
   lua_register(L, "createInterruptManager", _createInterruptManager);
   lua_register(L, "createSimulationEngine", _createSimulationEngine);
   lua_register(L, "appendInterruptManager", _appendInterruptManager);
   lua_register(L, "getIntSourcesCount", _getIntSourcesCount);
   lua_register(L, "getInterruptsCount", _getInterruptsCount);
   lua_register(L, "simulateInterrupt", _simulateInterrupt);
   lua_register(L, "restartEngine", _restartEngine);
   lua_register(L, "stepOverEngine", _stepOverEngine);
   lua_register(L, "getCurrentTime", _getCurrentTime);
   lua_register(L, "getFutureTime", _getFutureTime);
   lua_register(L, "getCurrentSource", _getCurrentSource);
   lua_register(L, "getFutureSource", _getFutureSource);
}
