/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef COMPONENTS_COMPONENTSSET_HPP
#define COMPONENTS_COMPONENTSSET_HPP


#include <cstddef>


#include "kernel/KernelObject.hpp"


/***************************************************************************
 *   ComponentsSet class declaration                                       *
 ***************************************************************************/


template <class T>
class ComponentsSet : public KernelObject
{
   public:
      explicit ComponentsSet(unsigned int count);
      virtual ~ComponentsSet();

      T & operator[](unsigned int index);
      const T & operator[](unsigned int index) const;

      T & at(unsigned int index);
      const T & at(unsigned int index) const;

      inline unsigned int count() const {return m_propertiesCount;}

   private:
      ComponentsSet(const ComponentsSet & other);
      ComponentsSet & operator=(const ComponentsSet & other);

      unsigned int m_propertiesCount;
      T * m_properties;
};


/***************************************************************************
 *   ComponentsSet class implementation                                    *
 ***************************************************************************/


template <class T>
ComponentsSet<T>::ComponentsSet(unsigned int count)
   : KernelObject(), m_propertiesCount(count), m_properties(NULL)
{
   if (count > 0)
   {
      m_properties = new T[count];
   }
}


template <class T>
ComponentsSet<T>::~ComponentsSet()
{
   if (m_properties) delete[] m_properties;
}


template <class T>
T & ComponentsSet<T>::operator[](unsigned int index)
{
   return m_properties[index];
}


template <class T>
const T & ComponentsSet<T>::operator[](unsigned int index) const
{
   return m_properties[index];
}


template <class T>
T & ComponentsSet<T>::at(unsigned int index)
{
   return m_properties[index];
}


template <class T>
const T & ComponentsSet<T>::at(unsigned int index) const
{
   return m_properties[index];
}


#endif
