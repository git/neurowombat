/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cassert>


#include "components/abstract/AbstractWeights.hpp"


/***************************************************************************
 *   AbstractWeights class implementation                                  *
 ***************************************************************************/


AbstractWeights::AbstractWeights(unsigned int count)
   : ComponentsSet<double>(count)
{
}


AbstractWeights::~AbstractWeights()
{
}


/***************************************************************************
 *   AbstractWeightsManager class implementation                           *
 ***************************************************************************/


AbstractWeightsManager::AbstractWeightsManager(
   Distribution * distribution,
   AbstractWeights * abstractWeights,
   CustomFunction * fixFunction
) : InterruptManager(
      abstractWeights ? abstractWeights->count() : 0,
      false,
      distribution
   ),
   m_abstractWeights(abstractWeights),
   m_fixFunction(fixFunction)
{
   // Capture object;
   if (m_abstractWeights) m_abstractWeights->capture();

   if (m_fixFunction)
   {
      m_fixFunction->capture();
   }
   else
   {
      // Create backup;
      unsigned int backupLength = m_abstractWeights->count();
      if (backupLength > 0)
      {
         m_backup.reserve(backupLength);
         for (unsigned int i = 0; i < backupLength; ++i)
         {
            m_backup.push_back(m_abstractWeights->at(i));
         }

         assert(m_backup.size() == backupLength);
      }
   }
}


AbstractWeightsManager::~AbstractWeightsManager()
{
   // Release captured object;
   if (m_abstractWeights) m_abstractWeights->release();
   if (m_fixFunction) m_fixFunction->release();
}


void AbstractWeightsManager::simulateInterrupt(unsigned int intSource)
{
   if (intSource < intSourcesCount() && m_abstractWeights)
   {
      m_abstractWeights->at(intSource) = 0.0;
   }
}


void AbstractWeightsManager::handleInterrupt()
{
   // Break up weight;
   int weightIndex = intSource();
   if (weightIndex >= 0 && m_abstractWeights)
   {
      m_abstractWeights->at(weightIndex) = 0.0;
   }

   // Pass control to base implementation;
   InterruptManager::handleInterrupt();
}


void AbstractWeightsManager::reinit()
{
   // Call base implementation;
   InterruptManager::reinit();

   if (m_fixFunction)
   {
      m_fixFunction->call();
   }
   else
   {
      // Restore weights from backup;
      for (unsigned int i = 0; i < m_abstractWeights->count(); ++i)
      {
         m_abstractWeights->at(i) = m_backup[i];
      }
   }
}
