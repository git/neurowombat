/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef COMPONENTS_ABSTRACT_ABSTRACTWEIGHTS_HPP
#define COMPONENTS_ABSTRACT_ABSTRACTWEIGHTS_HPP


#include <vector>


#include "components/ComponentsSet.hpp"
#include "engine/InterruptManager.hpp"
#include "objects/CustomFunction.hpp"


/***************************************************************************
 *   AbstractWeights class declaration                                     *
 ***************************************************************************/


class AbstractWeights : public ComponentsSet<double>
{
   public:
      explicit AbstractWeights(unsigned int count);
      virtual ~AbstractWeights();
};


/***************************************************************************
 *   AbstractWeightsManager class declaration                              *
 ***************************************************************************/


class AbstractWeightsManager : public InterruptManager
{
   public:
      AbstractWeightsManager(
         Distribution * distribution,
         AbstractWeights * abstractWeights,
         CustomFunction * fixFunction
      );

      virtual ~AbstractWeightsManager();

      virtual void simulateInterrupt(unsigned int intSource);
      virtual void handleInterrupt();
      virtual void reinit();

   private:
      AbstractWeights * m_abstractWeights;
      CustomFunction * m_fixFunction;
      std::vector<double> m_backup;
};


#endif
