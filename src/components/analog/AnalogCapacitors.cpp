/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cassert>


#include "components/analog/AnalogCapacitors.hpp"


/***************************************************************************
 *   AnalogCapacitors class implementation                                 *
 ***************************************************************************/


AnalogCapacitors::AnalogCapacitors(unsigned int count)
   : ComponentsSet<double>(count)
{
}


AnalogCapacitors::~AnalogCapacitors()
{
}


/***************************************************************************
 *   AnalogCapacitorsManager class implementation                          *
 ***************************************************************************/


AnalogCapacitorsManager::AnalogCapacitorsManager(
   Distribution * distribution,
   AnalogCapacitors * analogCapacitors,
   CustomFunction * fixFunction
) : InterruptManager(
      analogCapacitors ? analogCapacitors->count() : 0,
      false,
      distribution
   ),
   m_analogCapacitors(analogCapacitors),
   m_fixFunction(fixFunction)
{
   // Capture object;
   if (m_analogCapacitors) m_analogCapacitors->capture();

   if (m_fixFunction)
   {
      m_fixFunction->capture();
   }
   else
   {
      // Create backup;
      unsigned int backupLength = m_analogCapacitors->count();
      if (backupLength > 0)
      {
         m_backup.reserve(backupLength);
         for (unsigned int i = 0; i < backupLength; ++i)
         {
            m_backup.push_back(m_analogCapacitors->at(i));
         }

         assert(m_backup.size() == backupLength);
      }
   }
}


AnalogCapacitorsManager::~AnalogCapacitorsManager()
{
   // Release captured object;
   if (m_analogCapacitors) m_analogCapacitors->release();
   if (m_fixFunction) m_fixFunction->release();
}


void AnalogCapacitorsManager::simulateInterrupt(unsigned int intSource)
{
   if (intSource < intSourcesCount() && m_analogCapacitors)
   {
      m_analogCapacitors->at(intSource) = 0.0;
   }
}


void AnalogCapacitorsManager::handleInterrupt()
{
   // Break up capacitor;
   int capacitorIndex = intSource();
   if (capacitorIndex >= 0 && m_analogCapacitors)
   {
      m_analogCapacitors->at(capacitorIndex) = 0.0;
   }

   // Pass control to base implementation;
   InterruptManager::handleInterrupt();
}


void AnalogCapacitorsManager::reinit()
{
   // Call base implementation;
   InterruptManager::reinit();

   if (m_fixFunction)
   {
      m_fixFunction->call();
   }
   else
   {
      // Restore capacitances from backup;
      for (unsigned int i = 0; i < m_analogCapacitors->count(); ++i)
      {
         m_analogCapacitors->at(i) = m_backup[i];
      }
   }
}
