/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cassert>


#include "components/analog/AnalogResistors.hpp"


/***************************************************************************
 *   AnalogResistors class implementation                                  *
 ***************************************************************************/


AnalogResistors::AnalogResistors(unsigned int count)
   : ComponentsSet<double>(count)
{
}


AnalogResistors::~AnalogResistors()
{
}


/***************************************************************************
 *   AnalogResistorsManager class implementation                           *
 ***************************************************************************/


AnalogResistorsManager::AnalogResistorsManager(
   Distribution * distribution,
   AnalogResistors * analogResistors,
   CustomFunction * fixFunction
) : InterruptManager(
      analogResistors ? analogResistors->count() : 0,
      false,
      distribution
   ),
   m_analogResistors(analogResistors),
   m_fixFunction(fixFunction)
{
   // Capture object;
   if (m_analogResistors) m_analogResistors->capture();

   if (m_fixFunction)
   {
      m_fixFunction->capture();
   }
   else
   {
      // Create backup;
      unsigned int backupLength = m_analogResistors->count();
      if (backupLength > 0)
      {
         m_backup.reserve(backupLength);
         for (unsigned int i = 0; i < backupLength; ++i)
         {
            m_backup.push_back(m_analogResistors->at(i));
         }

         assert(m_backup.size() == backupLength);
      }
   }
}


AnalogResistorsManager::~AnalogResistorsManager()
{
   // Release captured object;
   if (m_analogResistors) m_analogResistors->release();
   if (m_fixFunction) m_fixFunction->release();
}


void AnalogResistorsManager::simulateInterrupt(unsigned int intSource)
{
   if (intSource < intSourcesCount() && m_analogResistors)
   {
      m_analogResistors->at(intSource) = 0.0;
   }
}


void AnalogResistorsManager::handleInterrupt()
{
   // Break up resistor;
   int resistorIndex = intSource();
   if (resistorIndex >= 0 && m_analogResistors)
   {
      m_analogResistors->at(resistorIndex) = 0.0;
   }

   // Pass control to base implementation;
   InterruptManager::handleInterrupt();
}


void AnalogResistorsManager::reinit()
{
   // Call base implementation;
   InterruptManager::reinit();

   if (m_fixFunction)
   {
      m_fixFunction->call();
   }
   else
   {
      // Restore resistances from backup;
      for (unsigned int i = 0; i < m_analogResistors->count(); ++i)
      {
         m_analogResistors->at(i) = m_backup[i];
      }
   }
}
