/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cassert>


#include "components/digital/MemoryModule.hpp"


/***************************************************************************
 *   MemoryModule class implementation                                     *
 ***************************************************************************/


MemoryModule::MemoryModule(unsigned int count)
   : ComponentsSet<double>(count)
{
}


MemoryModule::~MemoryModule()
{
}


/***************************************************************************
 *   MemoryModuleManager class implementation                              *
 ***************************************************************************/


MemoryModuleManager::MemoryModuleManager(
   Distribution * distribution,
   MemoryModule * memoryModule,
   CustomFunction * fixFunction
) : InterruptManager(
      memoryModule ? memoryModule->count() * 64 : 0,
      false,
      distribution
   ),
   m_memoryModule(memoryModule),
   m_fixFunction(fixFunction)
{
   // Capture object;
   if (m_memoryModule) m_memoryModule->capture();

   if (m_fixFunction)
   {
      m_fixFunction->capture();
   }
   else
   {
      // Create backup;
      unsigned backupLength = m_memoryModule->count();
      if (backupLength > 0)
      {
         m_backup.reserve(backupLength);
         for (unsigned int i = 0; i < backupLength; ++i)
         {
            m_backup.push_back(m_memoryModule->at(i));
         }

         assert(m_backup.size() == backupLength);
      }
   }
}


MemoryModuleManager::~MemoryModuleManager()
{
   // Release captured object;
   if (m_memoryModule) m_memoryModule->release();
   if (m_fixFunction) m_fixFunction->release();
}


void MemoryModuleManager::simulateInterrupt(unsigned int intSource)
{
   if (intSource < intSourcesCount() && m_memoryModule)
   {
      unsigned int wordIndex = intSource / 64;
      unsigned int bitInWordIndex = intSource % 64;
      unsigned char mask = ~(0x01 << (7 - bitInWordIndex % 8));
      double word = m_memoryModule->at(wordIndex);
      ((unsigned char *) & word)[bitInWordIndex / 8] &= mask;
      m_memoryModule->at(wordIndex) = word;
   }
}


void MemoryModuleManager::handleInterrupt()
{
   // Break up bit;
   int bitIndex = intSource();
   if (bitIndex >= 0 && m_memoryModule)
   {
      unsigned int wordIndex = bitIndex / 64;
      unsigned int bitInWordIndex = bitIndex % 64;
      unsigned char mask = ~(0x01 << (7 - bitInWordIndex % 8));
      double word = m_memoryModule->at(wordIndex);
      ((unsigned char *) & word)[bitInWordIndex / 8] &= mask;
      m_memoryModule->at(wordIndex) = word;
   }

   // Pass control to base implementation;
   InterruptManager::handleInterrupt();
}


void MemoryModuleManager::reinit()
{
   // Call base implementation;
   InterruptManager::reinit();

   if (m_fixFunction)
   {
      m_fixFunction->call();
   }
   else
   {
      // Restore words from backup;
      for (unsigned int i = 0; i < m_memoryModule->count(); ++i)
      {
         m_memoryModule->at(i) = m_backup[i];
      }
   }
}
