/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cassert>
#include <cstddef>


#include "engine/InterruptManager.hpp"


/***************************************************************************
 *   InterruptManager abstract class implementation                        *
 ***************************************************************************/


InterruptManager::InterruptManager(
   unsigned int intSourcesCount,
   bool unlimitedRegeneration,
   Distribution * distribution
) : m_interruptsCount(0),
   m_intSource(-1),
   m_lastIntSource(-1)
{
   if (intSourcesCount > 0)
   {
      // Set regeneration limit;
      m_unlimitedRegeneration = unlimitedRegeneration;

      // Copy distribution;
      m_distribution = distribution;

      // Capture object;
      if (m_distribution) m_distribution->capture();

      // Generate interrupts;
      m_interrupts.reserve(intSourcesCount);
      for (unsigned int i = 0; i < intSourcesCount; ++i)
      {
         m_interrupts.push_back(m_distribution->generateTime());
      }

      assert(m_interrupts.size() == intSourcesCount);

      // Find out interrupt source;
      findOutIntSource();
   }
   else
   {
      m_unlimitedRegeneration = false;
      m_distribution = NULL;
   }
}


InterruptManager::~InterruptManager()
{
   // Release captured object;
   if (m_distribution) m_distribution->release();
}


double InterruptManager::getInterrupt() const
{
   return ((m_intSource >= 0) ? m_interrupts[m_intSource] : -1.0);
}


unsigned int InterruptManager::intSourcesCount() const
{
   return m_interrupts.size();
}


void InterruptManager::handleInterrupt()
{
   if (m_intSource >= 0)
   {
      if (m_unlimitedRegeneration)
      {
         // Generate new interrupt for current source;
         m_interrupts[m_intSource] = m_distribution->generateTime();
      }
      else
      {
         // Mask current source;
         m_interrupts[m_intSource] = -1.0;
      }

      // Increment interrupts counter;
      ++m_interruptsCount;

      // Find out interrupt source;
      findOutIntSource();
   }
}


void InterruptManager::reinit()
{
   // Clear interrupts counter;
   m_interruptsCount = 0;

   // Generate interrupts;
   unsigned int intSourcesCount = m_interrupts.size();
   for (unsigned int i = 0; i < intSourcesCount; ++i)
   {
      m_interrupts[i] = m_distribution->generateTime();
   }

   // Clear int source;
   m_intSource = -1;

   // Find out interrupt source;
   findOutIntSource();
}


void InterruptManager::findOutIntSource()
{
   // Store last interrupt source;
   m_lastIntSource = m_intSource;

   double min = -1.0;
   m_intSource = -1;

   unsigned int i = 0;
   unsigned int intSourcesCount = m_interrupts.size();

   // Initialize minimum;
   while (i < intSourcesCount)
   {
      if (m_interrupts[i] >= 0.0)
      {
         min = m_interrupts[i];
         m_intSource = i;
         break;
      }

      ++i;
   }

   // Search for minimum;
   while (i < intSourcesCount)
   {
      if (m_interrupts[i] >= 0.0 && m_interrupts[i] < min)
      {
         min = m_interrupts[i];
         m_intSource = i;
      }

      ++i;
   }
}
