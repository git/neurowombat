/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef ENGINE_INTERRUPTMANAGER_HPP
#define ENGINE_INTERRUPTMANAGER_HPP


#include <vector>


#include "kernel/KernelObject.hpp"
#include "math/Distribution.hpp"


/***************************************************************************
 *   InterruptManager abstract class declaration                           *
 ***************************************************************************/


class InterruptManager : public KernelObject
{
   public:
      InterruptManager(
         unsigned int intSourcesCount,
         bool unlimitedRegeneration,
         Distribution * distribution
      );
      virtual ~InterruptManager();

      double getInterrupt() const;
      inline int lastIntSource() const {return m_lastIntSource;}
      inline int intSource() const {return m_intSource;}
      unsigned int intSourcesCount() const;
      inline unsigned int interruptsCount() const {return m_interruptsCount;}

      virtual void simulateInterrupt(unsigned int intSource) = 0;

      // Base method should be called at the end of reimplementation;
      virtual void handleInterrupt() = 0;

      // Base method should be called in reimplementation;
      virtual void reinit() = 0;

   private:
      // Most time-consuming operation;
      void findOutIntSource();

      unsigned int m_interruptsCount;

      std::vector<double> m_interrupts;

      int m_intSource;
      int m_lastIntSource;

      bool m_unlimitedRegeneration;
      Distribution * m_distribution;
};


#endif
