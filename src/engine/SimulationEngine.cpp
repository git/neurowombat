/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cstddef>


#include "engine/SimulationEngine.hpp"


/***************************************************************************
 *   SimulationEngine class implementation                                 *
 ***************************************************************************/


SimulationEngine::SimulationEngine()
   : m_currentTime(0.0), m_currentIntSource(NULL), m_futureIntSource(NULL)
{
}


SimulationEngine::~SimulationEngine()
{
   for (int i = m_managers.size() - 1; i >= 0; --i)
   {
      // Release captured object;
      if (m_managers[i]) m_managers[i]->release();
   }
}


void SimulationEngine::appendManager(InterruptManager * manager)
{
   if (!manager)
   {
      // throw SimulationEngineExcp(NS_SIMULATIONENGINE::NULL_PARAMETER);
   }

   // Capture object;
   if (manager) manager->capture();

   m_managers.push_back(manager);
}


void SimulationEngine::insertManagerBefore(unsigned int index, InterruptManager * manager)
{
   if (index >= m_managers.size())
   {
      // throw SimulationEngineExcp(NS_SIMULATIONENGINE::INDEX_OUT_OF_RANGE);
   }

   if (!manager)
   {
      // throw SimulationEngineExcp(NS_SIMULATIONENGINE::NULL_PARAMETER);
   }

   m_managers.insert(m_managers.begin() + index, manager);
}


void SimulationEngine::deleteManager(unsigned int index)
{
   if (index >= m_managers.size())
   {
      // throw SimulationEngineExcp(NS_SIMULATIONENGINE::INDEX_OUT_OF_RANGE);
   }

   // Release captured object;
   m_managers[index]->release();

   m_managers.erase(m_managers.begin() + index);
}


void SimulationEngine::clear()
{
   m_managers.clear();
   m_currentTime = 0.0;
   m_currentIntSource = NULL;
   m_futureIntSource = NULL;
}


void SimulationEngine::restart()
{
   // Reinit all the managers;
   for (int i = m_managers.size() - 1; i >= 0; --i)
   {
      if (m_managers[i]) m_managers[i]->reinit();
   }

   m_currentTime = 0.0;
   m_currentIntSource = NULL;
   m_futureIntSource = NULL;
}


bool SimulationEngine::stepOver()
{
   InterruptManager * manager = m_futureIntSource;
   if (!manager) manager = findOutIntSource();

   // Clear future interrupt source;
   m_futureIntSource = NULL;

   // Call manager's interrupt handler;
   if (manager)
   {
      m_currentTime = manager->getInterrupt();
      m_currentIntSource = manager;
      manager->handleInterrupt();
      return true;
   }

   return false;
}


double SimulationEngine::futureTime()
{
   if (!m_futureIntSource) m_futureIntSource = findOutIntSource();
   return (m_futureIntSource) ? m_futureIntSource->getInterrupt() : -1.0;
}


InterruptManager * SimulationEngine::futureIntSource()
{
   if (!m_futureIntSource) m_futureIntSource = findOutIntSource();
   return m_futureIntSource;
}


InterruptManager * SimulationEngine::findOutIntSource()
{
   double min = -1.0;
   int winner = -1;

   int i = m_managers.size() - 1;

   // Initialize minimum;
   while (i >= 0)
   {
      if (m_managers[i]->getInterrupt() >= 0.0)
      {
         min = m_managers[i]->getInterrupt();
         winner = i;
         break;
      }

      --i;
   }

   // Search for minimum;
   double currentInt = -1.0;
   while (i >= 0)
   {
      currentInt = m_managers[i]->getInterrupt();
      if (currentInt >= 0.0 && currentInt < min)
      {
         min = currentInt;
         winner = i;
      }

      --i;
   }

   return (winner >= 0) ? m_managers[winner] : NULL;
}
