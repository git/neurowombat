/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cstdlib>
#include <ctime>


#include "api/api.hpp"
#include "api/constants.hpp"
#include "kernel/Kernel.hpp"


/***************************************************************************
 *   KernelObjectTable class implementation                                *
 ***************************************************************************/


KernelObjectTable::KernelObjectTable()
   : m_idSearchRequired(false), m_idCount(0), m_currentId(MIN_ID)
{
}


KernelObjectTable::~KernelObjectTable()
{
   clear();
}


KernelObjectId KernelObjectTable::createId(KernelObject * object)
{
   if (m_idCount >= MAX_ID - MIN_ID + 1 || !object) return 0;

   if (m_idSearchRequired)
   {
      while (true)
      {
         if (m_objects.find(m_currentId) == m_objects.end()) break;

         if (m_currentId < MAX_ID)
         {
            ++m_currentId;
         }
         else
         {
            m_currentId = MIN_ID;
         }
      }
   }

   // Set id;
   KernelObjectId id = m_currentId;

   // Insert id and object pointer;
   m_objects.insert(std::make_pair(id, object));

   // Capture object;
   object->capture();

   // Increase current id;
   if (m_currentId < MAX_ID)
   {
      ++m_currentId;
   }
   else
   {
      m_currentId = MIN_ID;
      m_idSearchRequired = true;
   }

   // Increase id counter;
   ++m_idCount;

   return id;
}


void KernelObjectTable::closeId(KernelObjectId id)
{
   std::map<KernelObjectId, KernelObject *>::iterator entryIterator =
      m_objects.find(id);

   if (entryIterator == m_objects.end()) return;

   // Set object;
   KernelObject * object = entryIterator->second;

   // Erase entry;
   m_objects.erase(entryIterator);

   // Release object;
   object->release();

   // Decrease id counter;
   --m_idCount;
}


void KernelObjectTable::clear()
{
   m_idSearchRequired = false;
   m_idCount = 0;
   m_currentId = MIN_ID;

   std::map<KernelObjectId, KernelObject *>::iterator entryIterator =
      m_objects.begin();

   // Release all the objects;
   while (entryIterator != m_objects.end())
   {
      entryIterator->second->release();
      ++entryIterator;
   }

   // Clear objects;
   m_objects.clear();
}


KernelObject * KernelObjectTable::getObject(KernelObjectId id)
{
   std::map<KernelObjectId, KernelObject *>::iterator entryIterator =
      m_objects.find(id);

   if (entryIterator == m_objects.end()) return NULL;

   return entryIterator->second;
}


KernelObjectId KernelObjectTable::getId(const KernelObject * object)
{
   KernelObjectId id = 0;

   std::map<KernelObjectId, KernelObject *>::iterator entryIterator =
      m_objects.begin();

   // Try to find id;
   while (entryIterator != m_objects.end())
   {
      if (entryIterator->second == object)
      {
         id = entryIterator->first;
         break;
      }

      ++entryIterator;
   }

   return id;
}


/***************************************************************************
 *   Kernel singleton class implementation                                 *
 ***************************************************************************/


KernelObjectId Kernel::insertObject(KernelObject * object)
{
   // Try to create id;
   return m_objectTable.createId(object);
}


void Kernel::deleteObject(KernelObjectId id)
{
   // Try to close id;
   m_objectTable.closeId(id);
}


KernelObject * Kernel::getObject(KernelObjectId id)
{
   // Try to get object by it's id;
   return m_objectTable.getObject(id);
}


KernelObjectId Kernel::getId(const KernelObject * object)
{
   // Try to get id by object;
   return m_objectTable.getId(object);
}


bool Kernel::doFile(const char * fileName)
{
   if (m_luaVM)
   {
      if (!luaL_dofile(m_luaVM, fileName))
      {
         return true;
      }
      else
      {
         if (const char * error = lua_tostring(m_luaVM, -1))
         {
            printf("%s\n", error);
         }
      }
   }

   return false;
}


Kernel::Kernel()
   : Singleton<Kernel>::Singleton()
{
   // Initialize random generator;
   srand(time(NULL));

   // Create lua virtual machine context;
   m_luaVM = luaL_newstate();

   // Initialize lua standard library functions;
   luaL_openlibs(m_luaVM);

   // Set modules path;
   lua_getglobal(m_luaVM, "package");
   lua_getfield(m_luaVM, -1, "path");
   lua_pushstring(m_luaVM, ";" MODULES_PATH);
   lua_concat(m_luaVM, 2);
   lua_setfield(m_luaVM, -2, "path");
   lua_pop(m_luaVM, 1);

   // Register API functions;
   registerApiFunctions(m_luaVM);

   // Register API constants;
   registerApiConstants(m_luaVM);
}


Kernel::~Kernel()
{
   // Close lua virtual machine context;
   lua_close(m_luaVM);
}
