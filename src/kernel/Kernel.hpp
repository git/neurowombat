/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef KERNEL_KERNEL_HPP
#define KERNEL_KERNEL_HPP


#include <map>


#include <lua.hpp>


#include "kernel/KernelObject.hpp"
#include "patterns/Singleton.hpp"


/***************************************************************************
 *   KernelObjectTable class declaration                                   *
 ***************************************************************************/


class KernelObjectTable
{
   public:
      enum CONSTANTS
      {
         MIN_ID = 0x00000001,
         MAX_ID = 0xFFFFFFFF
      };

      KernelObjectTable();
      virtual ~KernelObjectTable();

      // This function is NULL safe;
      KernelObjectId createId(KernelObject * object);
      void closeId(KernelObjectId id);
      void clear();

      KernelObject * getObject(KernelObjectId id);
      KernelObjectId getId(const KernelObject * object);
   private:
      bool m_idSearchRequired;
      unsigned int m_idCount;
      KernelObjectId m_currentId;

      std::map<KernelObjectId, KernelObject *> m_objects;
};


/***************************************************************************
 *   Kernel singleton class declaration                                    *
 ***************************************************************************/


class Kernel : public Singleton<Kernel>
{
   friend class Singleton<Kernel>;

   public:
      // This function is NULL safe;
      KernelObjectId insertObject(KernelObject * object);
      void deleteObject(KernelObjectId id);
      KernelObject * getObject(KernelObjectId id);
      KernelObjectId getId(const KernelObject * object);

      inline lua_State * getVM() const {return m_luaVM;}
      bool doFile(const char * fileName);

   private:
      Kernel();
      virtual ~Kernel();

      KernelObjectTable m_objectTable;
      lua_State * m_luaVM;
};


#endif
