/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cmath>


#include "ActivationFunction.hpp"


/***************************************************************************
 *   ActivationFunction abstract class implementation                      *
 ***************************************************************************/


ActivationFunction::ActivationFunction()
   : KernelObject()
{
}


ActivationFunction::~ActivationFunction()
{
}


/***************************************************************************
 *   CustomActivationFunction class implementation                         *
 ***************************************************************************/


CustomActivationFunction::CustomActivationFunction(
   CustomFunction * customFunction,
   CustomFunction * customDerivative
) : ActivationFunction(),
   m_customFunction(customFunction),
   m_customDerivative(customDerivative)
{
   if (m_customFunction) m_customFunction->capture();
   if (m_customDerivative) m_customDerivative->capture();
}


CustomActivationFunction::~CustomActivationFunction()
{
   if (m_customFunction) m_customFunction->release();
   if (m_customDerivative) m_customDerivative->release();
}


double CustomActivationFunction::evaluateFunction(double x)
{
   return m_customFunction->call(x);
}


double CustomActivationFunction::evaluateDerivative(double x)
{
   return m_customDerivative->call(x);
}


/***************************************************************************
 *   GaussianActivationFunction class implementation                       *
 ***************************************************************************/


GaussianActivationFunction::GaussianActivationFunction(double beta)
   : ActivationFunction(), m_beta(beta)
{
}


GaussianActivationFunction::~GaussianActivationFunction()
{
}


double GaussianActivationFunction::evaluateFunction(double x)
{
   return exp(-m_beta * x * x);
}


double GaussianActivationFunction::evaluateDerivative(double x)
{
   return -2.0 * x * m_beta * exp(-m_beta * x * x);
}


/***************************************************************************
 *   LimActivationFunction class implementation                            *
 ***************************************************************************/


LimActivationFunction::LimActivationFunction(
   double xLim,
   double yLow,
   double yHigh
) : ActivationFunction(), m_xLim(xLim), m_yLow(yLow), m_yHigh(yHigh)
{
}


LimActivationFunction::~LimActivationFunction()
{
}


double LimActivationFunction::evaluateFunction(double x)
{
   return (x < m_xLim) ? m_yLow : m_yHigh;
}


double LimActivationFunction::evaluateDerivative(double x)
{
   return 0.0;
}


/***************************************************************************
 *   LinearActivationFunction class implementation                         *
 ***************************************************************************/


LinearActivationFunction::LinearActivationFunction(double a, double b)
   : ActivationFunction(), m_a(a), m_b(b)
{
}


LinearActivationFunction::~LinearActivationFunction()
{
}


double LinearActivationFunction::evaluateFunction(double x)
{
   return m_a * x + m_b;
}


double LinearActivationFunction::evaluateDerivative(double x)
{
   return m_a;
}


/***************************************************************************
 *   LimLinearActivationFunction class implementation                      *
 ***************************************************************************/


LimLinearActivationFunction::LimLinearActivationFunction(
   double a,
   double b,
   double xMin,
   double xMax
) : ActivationFunction(), m_a(a), m_b(b), m_xMin(xMin), m_xMax(xMax)
{
}


LimLinearActivationFunction::~LimLinearActivationFunction()
{
}


double LimLinearActivationFunction::evaluateFunction(double x)
{
   if (x < m_xMin)
   {
      return m_a * m_xMin + m_b;
   }
   else if (x > m_xMax)
   {
      return m_a * m_xMax + m_b;
   }

   return m_a * x + m_b;
}


double LimLinearActivationFunction::evaluateDerivative(double x)
{
   if (x < m_xMin || x > m_xMax)
   {
      return 0.0;
   }

   return m_a;
}


/***************************************************************************
 *   PosLinearActivationFunction class implementation                      *
 ***************************************************************************/


PosLinearActivationFunction::PosLinearActivationFunction(double a, double b)
   : ActivationFunction(), m_a(a), m_b(b)
{
}


PosLinearActivationFunction::~PosLinearActivationFunction()
{
}


double PosLinearActivationFunction::evaluateFunction(double x)
{
   double f = m_a * x + m_b;
   return (f >= 0.0) ? f : 0.0;
}


double PosLinearActivationFunction::evaluateDerivative(double x)
{
   return (m_a * x + m_b >= 0.0) ?  m_a : 0.0;
}


/***************************************************************************
 *   SigmoidActivationFunction class implementation                        *
 ***************************************************************************/


SigmoidActivationFunction::SigmoidActivationFunction(double lambda)
   : ActivationFunction(), m_lambda(lambda)
{
}


SigmoidActivationFunction::~SigmoidActivationFunction()
{
}


double SigmoidActivationFunction::evaluateFunction(double x)
{
   return (1.0 / (1.0 + exp(-m_lambda * x)));
}


double SigmoidActivationFunction::evaluateDerivative(double x)
{
   double f = 1.0 / (1.0 + exp(-m_lambda * x));
   return m_lambda * f * (1 - f);
}


/***************************************************************************
 *   ThSigmoidActivationFunction class implementation                      *
 ***************************************************************************/


ThSigmoidActivationFunction::ThSigmoidActivationFunction()
   : ActivationFunction()
{
}


ThSigmoidActivationFunction::~ThSigmoidActivationFunction()
{
}


double ThSigmoidActivationFunction::evaluateFunction(double x)
{
   return tanh(x);
}


double ThSigmoidActivationFunction::evaluateDerivative(double x)
{
   double f = cosh(x);
   return 1.0 / (f * f);
}
