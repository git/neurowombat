/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cmath>
#include <cstdlib>


#include "math/Distribution.hpp"


/***************************************************************************
 *   Distribution abstract class implementation                            *
 ***************************************************************************/


Distribution::Distribution()
{
}


Distribution::~Distribution()
{
}


inline double Distribution::genUniformRandomValue()
{
   return (static_cast<double>(rand()) / static_cast<double>(RAND_MAX));
}


/***************************************************************************
 *   CustomDistribution class implementation                               *
 ***************************************************************************/


CustomDistribution::CustomDistribution(CustomFunction * customInverseFunction)
   : Distribution(), m_customInverseFunction(customInverseFunction)
{
   if (m_customInverseFunction) m_customInverseFunction->capture();
}


CustomDistribution::~CustomDistribution()
{
   if (m_customInverseFunction) m_customInverseFunction->release();
}


double CustomDistribution::generateTime()
{
   return m_customInverseFunction->call(genUniformRandomValue());
}


/***************************************************************************
 *   ExponentialDistribution class implementation                          *
 ***************************************************************************/


ExponentialDistribution::ExponentialDistribution(double lambda)
   : Distribution(), m_lambda(lambda)
{
}


ExponentialDistribution::~ExponentialDistribution()
{
}


double ExponentialDistribution::generateTime()
{
   return (-log(1.0 - genUniformRandomValue())) / m_lambda;
}


/***************************************************************************
 *   WeibullDistribution class implementation                              *
 ***************************************************************************/


WeibullDistribution::WeibullDistribution(double theta, double beta)
   : Distribution(), m_theta(theta), m_beta(beta)
{
}


WeibullDistribution::~WeibullDistribution()
{
}


double WeibullDistribution::generateTime()
{
   return pow(-log(1.0 - genUniformRandomValue()) / m_theta, 1.0 / m_beta);
}
