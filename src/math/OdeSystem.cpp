/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "math/OdeSystem.hpp"


/***************************************************************************
 *   OdeSystem abstract class implementation                               *
 ***************************************************************************/


OdeSystem::OdeSystem(unsigned int equationsCount)
   : m_equationsCount(equationsCount), m_depVars(NULL)
{
   if (m_equationsCount > 0)
   {
      m_depVars = new double[m_equationsCount];
   }
}


OdeSystem::~OdeSystem()
{
   if (m_depVars) delete[] m_depVars;
}


void OdeSystem::setIndependentVariable(double x)
{
   m_indepVar = x;
}


void OdeSystem::incIndependentVariable(double x)
{
   m_indepVar += x;
}


void OdeSystem::setDependentVariable(unsigned int index, double x)
{
   m_depVars[index] = x;
}


double OdeSystem::getDependentVariable(unsigned int index)
{
   return m_depVars[index];
}


void OdeSystem::incDependentVariable(unsigned int index, double x)
{
   m_depVars[index] += x;
}


void OdeSystem::incDependentVariables(double * x1, double * x2, double k1, double k2)
{
   for (unsigned int i = 0; i < m_equationsCount; ++i)
   {
      m_depVars[i] += k1 * x1[i] + k2 * x2[i];
   }
}


/***************************************************************************
 *   AbstractNeuronsOdeSystem class implementation                         *
 ***************************************************************************/


AbstractNeuronsOdeSystem::AbstractNeuronsOdeSystem(
   std::vector<AbstractNeuron *> * neurons,
   double timeConstant
) : OdeSystem(neurons ? neurons->size() : 0),
   m_neurons(neurons),
   m_timeConstant(timeConstant)
{
   // Initialize independent variable;
   m_indepVar = 0.0;

   // Initialize dependent variables;
   for (unsigned int i = 0; i < m_equationsCount; ++i)
   {
      m_depVars[i] = m_neurons->at(i)->leftCompute();
   }
}


AbstractNeuronsOdeSystem::~AbstractNeuronsOdeSystem()
{
}


double AbstractNeuronsOdeSystem::evaluateOdeFunction(unsigned int index)
{
   return (-m_depVars[index] / m_timeConstant) + m_neurons->at(index)->leftCompute();
}


void AbstractNeuronsOdeSystem::setDependentVariable(unsigned int index, double x)
{
   m_depVars[index] = x;
   m_neurons->at(index)->rightCompute(m_depVars[index]);
}


void AbstractNeuronsOdeSystem::incDependentVariable(unsigned int index, double x)
{
   m_depVars[index] += x;
   m_neurons->at(index)->rightCompute(m_depVars[index]);
}


void AbstractNeuronsOdeSystem::incDependentVariables(double * x1, double * x2, double k1, double k2)
{
   for (unsigned int i = 0; i < m_equationsCount; ++i)
   {
      m_depVars[i] += k1 * x1[i] + k2 * x2[i];
      m_neurons->at(i)->rightCompute(m_depVars[i]);
   }
}


/***************************************************************************
 *   AnalogLimNeuronsOdeSystem class implementation                        *
 ***************************************************************************/


AnalogLimNeuronsOdeSystem::AnalogLimNeuronsOdeSystem(
   std::vector<AnalogNeuron *> * neurons
) : OdeSystem(neurons ? 2 * neurons->size() : 0),
   m_neurons(neurons)
{
   // Initialize independent variable;
   m_indepVar = 0.0;

   // Initialize dependent variables;
   for (unsigned int i = 0; i < m_neurons->size(); ++i)
   {
      m_depVars[i] = m_neurons->at(i)->leftComputePos();
      m_depVars[i + m_neurons->size()] = m_neurons->at(i)->leftComputeNeg();
   }
}


AnalogLimNeuronsOdeSystem::~AnalogLimNeuronsOdeSystem()
{
}


double AnalogLimNeuronsOdeSystem::evaluateOdeFunction(unsigned int index)
{
   double result = 0.0;
   if (index < m_neurons->size())
   {
      result = m_neurons->at(index)->getPosConstant();
      if (result != 0.0) result = (-m_depVars[index] + m_neurons->at(index)->leftComputePos()) / result;
   }
   else
   {
      result = m_neurons->at(index - m_neurons->size())->getNegConstant();
      if (result != 0.0) result = (-m_depVars[index] + m_neurons->at(index - m_neurons->size())->leftComputeNeg()) / result;
   }

   return result;
}


void AnalogLimNeuronsOdeSystem::setDependentVariable(unsigned int index, double x)
{
   m_depVars[index] = x;

   if (index < m_neurons->size())
   {
      m_neurons->at(index)->rightCompute(m_depVars[index + m_neurons->size()], m_depVars[index]);
   }
   else
   {
      m_neurons->at(index - m_neurons->size())->rightCompute(m_depVars[index], m_depVars[index - m_neurons->size()]);
   }
}


void AnalogLimNeuronsOdeSystem::incDependentVariable(unsigned int index, double x)
{
   m_depVars[index] += x;

   if (index < m_neurons->size())
   {
      m_neurons->at(index)->rightCompute(m_depVars[index + m_neurons->size()], m_depVars[index]);
   }
   else
   {
      m_neurons->at(index - m_neurons->size())->rightCompute(m_depVars[index], m_depVars[index - m_neurons->size()]);
   }
}


void AnalogLimNeuronsOdeSystem::incDependentVariables(double * x1, double * x2, double k1, double k2)
{
   for (unsigned int i = 0; i < m_equationsCount; ++i)
   {
      m_depVars[i] += k1 * x1[i] + k2 * x2[i];
      if (i < m_neurons->size())
      {
         m_neurons->at(i)->rightCompute(m_depVars[i + m_neurons->size()], m_depVars[i]);
      }
      else
      {
         m_neurons->at(i - m_neurons->size())->rightCompute(m_depVars[i], m_depVars[i - m_neurons->size()]);
      }
   }
}
