/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef MATH_ODESYSTEM_HPP
#define MATH_ODESYSTEM_HPP


#include <vector>


#include "neurons/abstract/AbstractNeuron.hpp"
#include "neurons/analog/AnalogNeuron.hpp"


/***************************************************************************
 *   OdeSystem abstract class declaration                                  *
 ***************************************************************************/


class OdeSystem
{
   public:
      explicit OdeSystem(unsigned int equationsCount);
      virtual ~OdeSystem();

      inline unsigned int equationsCount() {return m_equationsCount;}

      virtual double evaluateOdeFunction(unsigned int index) = 0;

      void setIndependentVariable(double x);
      inline double independentVariable() {return m_indepVar;}
      void incIndependentVariable(double x);

      virtual void setDependentVariable(unsigned int index, double x);
      double getDependentVariable(unsigned int index);
      virtual void incDependentVariable(unsigned int index, double x);
      virtual void incDependentVariables(double * x1, double * x2, double k1, double k2);

   private:
      OdeSystem(const OdeSystem & other);
      OdeSystem & operator=(const OdeSystem & other);

   protected:
      unsigned int m_equationsCount;
      double m_indepVar;
      double * m_depVars;
};


/***************************************************************************
 *   AbstractNeuronsOdeSystem class declaration                            *
 ***************************************************************************/


class AbstractNeuronsOdeSystem : public OdeSystem
   {
   public:
      AbstractNeuronsOdeSystem(
         std::vector<AbstractNeuron *> * neurons,
         double timeConstant
      );

      virtual ~AbstractNeuronsOdeSystem();

      virtual double evaluateOdeFunction(unsigned int index);

      virtual void setDependentVariable(unsigned int index, double x);
      virtual void incDependentVariable(unsigned int index, double x);
      virtual void incDependentVariables(double * x1, double * x2, double k1, double k2);

   private:
      std::vector<AbstractNeuron *> * m_neurons;
      double m_timeConstant;
   };


/***************************************************************************
 *   AnalogLimNeuronsOdeSystem class declaration                           *
 ***************************************************************************/


class AnalogLimNeuronsOdeSystem : public OdeSystem
   {
   public:
      explicit AnalogLimNeuronsOdeSystem(std::vector<AnalogNeuron *> * neurons);

      virtual ~AnalogLimNeuronsOdeSystem();

      virtual double evaluateOdeFunction(unsigned int index);

      virtual void setDependentVariable(unsigned int index, double x);
      virtual void incDependentVariable(unsigned int index, double x);
      virtual void incDependentVariables(double * x1, double * x2, double k1, double k2);

   private:
      std::vector<AnalogNeuron *> * m_neurons;
   };


#endif
