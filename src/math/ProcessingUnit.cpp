/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cmath>


#include <lua.hpp>


#include "kernel/Kernel.hpp"
#include "math/ProcessingUnit.hpp"


// It is better for API functions to use this pointer instead of
// Kernel::instance() and Kernel::freeInstance() methods;
extern Kernel * kernel;


double useCoefficient(COEFF_USAGE::T_COEFF_USAGE coeffUsage, double x, double c)
{
   switch (coeffUsage)
   {
      case COEFF_USAGE::NOP:
         break;
      case COEFF_USAGE::ADD_TO:
         x += c;
         break;
      case COEFF_USAGE::MUL_BY:
         x *= c;
         break;
      case COEFF_USAGE::SUB_IT_FROM:
         x -= c;
         break;
      case COEFF_USAGE::SUB_FROM_IT:
         x = c - x;
         break;
      case COEFF_USAGE::DIV_IT_BY:
         x = c / x;
         break;
      case COEFF_USAGE::DIV_BY_IT:
         x /= c;
         break;
   }

   return x;
}


/***************************************************************************
 *   ProcessingUnit abstract class implementation                          *
 ***************************************************************************/


ProcessingUnit::ProcessingUnit()
   : KernelObject()
{
}


ProcessingUnit::~ProcessingUnit()
{
}


/***************************************************************************
 *   CustomProcessingUnit class implementation                             *
 ***************************************************************************/


CustomProcessingUnit::CustomProcessingUnit(CustomFunction * processingFunc)
   : ProcessingUnit(), m_processingFunc(processingFunc)
{
   if (m_processingFunc) m_processingFunc->capture();
}


CustomProcessingUnit::~CustomProcessingUnit()
{
   if (m_processingFunc) m_processingFunc->release();
}


double CustomProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   AbstractConnectors * connectors,
   double * builtInWeights,
   AbstractWeights * weights,
   unsigned int weightsBaseIndex
)
{
   const unsigned int inputsCount = inputConnectors.size();

   lua_State * L = kernel->getVM();
   lua_rawgeti(L, LUA_REGISTRYINDEX, m_processingFunc->functionReference());

   // Push signals array;
   lua_newtable(L);
   for (unsigned int i = 0; i < inputsCount; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, connectors->at(inputConnectors[i]));
      lua_rawset(L, -3);
   }

   // Push weights array;
   if (weights)
   {
      // Use external weights;
      lua_newtable(L);
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         // Increase key by 1 to provide compatibility between C and Lua-style arrays;
         lua_pushnumber(L, i + 1);
         lua_pushnumber(L, weights->at(weightsBaseIndex + i));
         lua_rawset(L, -3);
      }
   }
   else
   {
      // Use build-in weights;
      lua_newtable(L);
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         // Increase key by 1 to provide compatibility between C and Lua-style arrays;
         lua_pushnumber(L, i + 1);
         lua_pushnumber(L, builtInWeights[i]);
         lua_rawset(L, -3);
      }
   }

   lua_pcall(L, 2, 1, 0);
   double result = lua_tonumber(L, -1);
   lua_pop(L, 1);
   return result;
}


double CustomProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   DigitalConnectors * connectors,
   MemoryModule * memory,
   unsigned int memoryBaseIndex
)
{
   const unsigned int inputsCount = inputConnectors.size();

   lua_State * L = kernel->getVM();
   lua_rawgeti(L, LUA_REGISTRYINDEX, m_processingFunc->functionReference());

   // Push signals array;
   lua_newtable(L);
   for (unsigned int i = 0; i < inputsCount; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, connectors->at(inputConnectors[i]));
      lua_rawset(L, -3);
   }

   // Push weights array;
   lua_newtable(L);
   for (unsigned int i = 0; i < inputsCount; ++i)
   {
      // Increase key by 1 to provide compatibility between C and Lua-style arrays;
      lua_pushnumber(L, i + 1);
      lua_pushnumber(L, memory->at(memoryBaseIndex + i));
      lua_rawset(L, -3);
   }

   lua_pcall(L, 2, 1, 0);
   double result = lua_tonumber(L, -1);
   lua_pop(L, 1);
   return result;
}


/***************************************************************************
 *   RadialBasisProcessingUnit class implementation                        *
 ***************************************************************************/


RadialBasisProcessingUnit::RadialBasisProcessingUnit(
   COEFF_USAGE::T_COEFF_USAGE coeffUsage
) : ProcessingUnit(), m_coeffUsage(coeffUsage)
{
}


RadialBasisProcessingUnit::~RadialBasisProcessingUnit()
{
}


double RadialBasisProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   AbstractConnectors * connectors,
   double * builtInWeights,
   AbstractWeights * weights,
   unsigned int weightsBaseIndex
)
{
   unsigned int inputsCount = inputConnectors.size();

   if (m_coeffUsage != COEFF_USAGE::NOP) inputsCount --;

   // Calculate distance between input signals and weights;
   double dist = 0.0;
   double buffer = 0.0;
   if (weights)
   {
      // Use external weights;
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         buffer = weights->at(weightsBaseIndex + i) - connectors->at(inputConnectors[i]);
         dist += buffer * buffer;
      }

      if (m_coeffUsage != COEFF_USAGE::NOP)
      {
         buffer = weights->at(weightsBaseIndex + inputsCount) * connectors->at(inputConnectors[inputsCount]);
      }
   }
   else
   {
      // Use build-in weights;
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         buffer = builtInWeights[i] - connectors->at(inputConnectors[i]);
         dist += buffer * buffer;
      }

      if (m_coeffUsage != COEFF_USAGE::NOP)
      {
         buffer = builtInWeights[inputsCount] * connectors->at(inputConnectors[inputsCount]);
      }
   }

   dist = useCoefficient(m_coeffUsage, dist, buffer);
   dist = sqrt(dist);

   return dist;
}


double RadialBasisProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   DigitalConnectors * connectors,
   MemoryModule * memory,
   unsigned int memoryBaseIndex
)
{
   unsigned int inputsCount = inputConnectors.size();

   if (m_coeffUsage != COEFF_USAGE::NOP) inputsCount --;

   // Calculate distance between input signals and weights;
   double dist = 0.0;
   double buffer = 0.0;
   for (unsigned int i = 0; i < inputsCount; ++i)
   {
      buffer = memory->at(memoryBaseIndex + i) - connectors->at(inputConnectors[i]);
      dist += buffer * buffer;
   }

   if (m_coeffUsage != COEFF_USAGE::NOP)
   {
      buffer = memory->at(memoryBaseIndex + inputsCount) * connectors->at(inputConnectors[inputsCount]);
   }

   dist = useCoefficient(m_coeffUsage, dist, buffer);
   dist = sqrt(dist);

   return dist;
}


/***************************************************************************
 *   ScalarProcessingUnit class implementation                             *
 ***************************************************************************/


ScalarProcessingUnit::ScalarProcessingUnit()
   : ProcessingUnit()
{
}


ScalarProcessingUnit::~ScalarProcessingUnit()
{
}


double ScalarProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   AbstractConnectors * connectors,
   double * builtInWeights,
   AbstractWeights * weights,
   unsigned int weightsBaseIndex
)
{
   const unsigned int inputsCount = inputConnectors.size();

   // Calculate normalized scalar product of input signals and weights;
   double buffer = 0.0;
   double product = 0.0;
   double sum = 0.0;
   if (weights)
   {
      // Use external weights;
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         buffer = connectors->at(inputConnectors[i]);
         product += weights->at(weightsBaseIndex + i) * buffer;
         sum += buffer;
      }
   }
   else
   {
      // Use build-in weights;
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         buffer = connectors->at(inputConnectors[i]);
         product += builtInWeights[i] * buffer;
         sum += buffer;
      }
   }

   return (sum != 0.0) ? product / sum : 0.0;
}


double ScalarProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   DigitalConnectors * connectors,
   MemoryModule * memory,
   unsigned int memoryBaseIndex
)
{
   const unsigned int inputsCount = inputConnectors.size();

   // Calculate normalized scalar product of input signals and weights;
   double buffer = 0.0;
   double product = 0.0;
   double sum = 0.0;
   for (unsigned int i = 0; i < inputsCount; ++i)
   {
      buffer = connectors->at(inputConnectors[i]);
      product += memory->at(memoryBaseIndex + i) * buffer;
      sum += buffer;
   }

   return (sum != 0.0) ? product / sum : 0.0;
}


/***************************************************************************
 *   WeightedSumProcessingUnit class implementation                        *
 ***************************************************************************/


WeightedSumProcessingUnit::WeightedSumProcessingUnit()
   : ProcessingUnit()
{
}


WeightedSumProcessingUnit::~WeightedSumProcessingUnit()
{
}


double WeightedSumProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   AbstractConnectors * connectors,
   double * builtInWeights,
   AbstractWeights * weights,
   unsigned int weightsBaseIndex
)
{
   const unsigned int inputsCount = inputConnectors.size();

   // Calculate weighted sum of input signals;
   double net = 0.0;
   if (weights)
   {
      // Use external weights;
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         net += weights->at(weightsBaseIndex + i) * connectors->at(inputConnectors[i]);
      }
   }
   else
   {
      // Use build-in weights;
      for (unsigned int i = 0; i < inputsCount; ++i)
      {
         net += builtInWeights[i] *
            connectors->at(inputConnectors[i]);
      }
   }

   return net;
}


double WeightedSumProcessingUnit::process(
   const std::vector<unsigned int> & inputConnectors,
   DigitalConnectors * connectors,
   MemoryModule * memory,
   unsigned int memoryBaseIndex
)
{
   const unsigned int inputsCount = inputConnectors.size();

   // Calculate weighted sum of input signals;
   double net = 0.0;
   for (unsigned int i = 0; i < inputsCount; ++i)
   {
      net += memory->at(memoryBaseIndex + i) * connectors->at(inputConnectors[i]);
   }

   return net;
}
