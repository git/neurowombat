/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "neurons/abstract/AbstractNeuron.hpp"


/***************************************************************************
 *   AbstractNeuron class implementation                                   *
 ***************************************************************************/


AbstractNeuron::AbstractNeuron(
   const std::vector<unsigned int> & inputConnectors,
   AbstractConnectors * connectors,
   unsigned int connectorsBaseIndex,
   AbstractWeights * weights,
   unsigned int weightsBaseIndex,
   ProcessingUnit * processingUnit,
   ActivationFunction * activationFunction
) : KernelObject(),
   m_inputConnectors(inputConnectors),
   m_connectors(connectors),
   m_connectorsBaseIndex(connectorsBaseIndex),
   m_builtInWeights(NULL),
   m_weights(weights),
   m_weightsBaseIndex(weightsBaseIndex),
   m_processingUnit(processingUnit),
   m_activationFunction(activationFunction),
   m_processingUnitOut(0.0),
   m_delta(0.0)
{
   // Capture objects;
   if (m_connectors) m_connectors->capture();
   if (m_weights) m_weights->capture();
   if (m_processingUnit) m_processingUnit->capture();
   if (m_activationFunction) m_activationFunction->capture();

   // Try to create built-in weights;
   if (!m_weights && !m_inputConnectors.empty())
   {
      m_builtInWeights = new double[m_inputConnectors.size()];
   }
}


AbstractNeuron::~AbstractNeuron()
{
   if (m_builtInWeights) delete[] m_builtInWeights;

   // Release captured objects;
   if (m_connectors) m_connectors->release();
   if (m_weights) m_weights->release();
   if (m_activationFunction) m_activationFunction->release();
   if (m_processingUnit) m_processingUnit->release();
}


unsigned int AbstractNeuron::getInputsCount() const
{
   return m_inputConnectors.size();
}


void AbstractNeuron::setWeight(unsigned int index, double weight)
{
   if (m_weights)
   {
      // Set external weight;
      m_weights->at(m_weightsBaseIndex + index) = weight;
   }
   else
   {
      // Set built-in weight;
      m_builtInWeights[index] = weight;
   }
}


double AbstractNeuron::getWeight(unsigned int index) const
{
   if (m_weights)
   {
      // Return external weights;
      return m_weights->at(m_weightsBaseIndex + index);
   }
   else
   {
      // Return built-in weights;
      return m_builtInWeights[index];
   }
}


double AbstractNeuron::getOutput() const
{
   return m_connectors->at(m_connectorsBaseIndex);
}


double AbstractNeuron::leftCompute()
{
   // Calculate processor out;
   m_processingUnitOut = m_processingUnit->process(
      m_inputConnectors, m_connectors,
      m_builtInWeights, m_weights, m_weightsBaseIndex
   );

   return m_processingUnitOut;
}


void AbstractNeuron::rightCompute(double processingUnitOut)
{
   m_processingUnitOut = processingUnitOut;
   m_connectors->at(m_connectorsBaseIndex) =
      m_activationFunction->evaluateFunction(m_processingUnitOut);
}


void AbstractNeuron::compute()
{
   // Calculate processor out;
   m_processingUnitOut = m_processingUnit->process(
      m_inputConnectors, m_connectors,
      m_builtInWeights, m_weights, m_weightsBaseIndex
   );

   m_connectors->at(m_connectorsBaseIndex) =
      m_activationFunction->evaluateFunction(m_processingUnitOut);
}


void AbstractNeuron::createDampingBuffers()
{
   if (m_builtInBuffers.empty())
   {
      m_builtInBuffers.resize(m_inputConnectors.size(), 0.0);
   }
}


void AbstractNeuron::snapDelta(double err)
{
   m_delta = err;
   m_delta *= m_activationFunction->evaluateDerivative(m_processingUnitOut);
}


double AbstractNeuron::getWeightedDelta(unsigned int index) const
{
   if (m_weights)
   {
      // Use external weights;
      return m_delta * m_weights->at(m_weightsBaseIndex + index);
   }
   else
   {
      // Use build-in weights;
      return m_delta * m_builtInWeights[index];
   }
}


void AbstractNeuron::modifyWeights(double damping, double speed)
{
   double d = m_delta * speed;
   double dw = 0;

   if (m_weights)
   {
      // Use external weights;
      for (unsigned int i = 0, icount = m_inputConnectors.size(); i < icount; ++i)
      {
         dw = damping * m_builtInBuffers[i] +
            d * m_connectors->at(m_inputConnectors[i]);
         m_builtInBuffers[i] = dw;
         m_weights->at(m_weightsBaseIndex + i) += dw;
      }
   }
   else
   {
      // Use build-in weights;
      for (unsigned int i = 0, icount = m_inputConnectors.size(); i < icount; ++i)
      {
         dw = damping * m_builtInBuffers[i] +
            d * m_connectors->at(m_inputConnectors[i]);
         m_builtInBuffers[i] = dw;
         m_builtInWeights[i] += dw;
      }
   }
}
