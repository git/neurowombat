/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include <cmath>


#include "neurons/analog/AnalogNeuron.hpp"


/***************************************************************************
 *   AnalogNeuron class implementation                                     *
 ***************************************************************************/


AnalogNeuron::AnalogNeuron(
   const std::vector<unsigned int> & inputWires,
   unsigned int gndWireIndex,
   unsigned int srcWireIndex,
   AnalogCapacitors * capacitors,
   unsigned int capacitorsBaseIndex,
   AnalogComparators * comparators,
   unsigned int comparatorsBaseIndex,
   AnalogResistors * resistors,
   unsigned int resistorsBaseIndex,
   AnalogWires * wires,
   unsigned int wiresBaseIndex
) : KernelObject(),
   m_inputWires(inputWires),
   m_gndWireIndex(gndWireIndex),
   m_srcWireIndex(srcWireIndex),
   m_capacitors(capacitors),
   m_capacitorsBaseIndex(capacitorsBaseIndex),
   m_comparators(comparators),
   m_comparatorsBaseIndex(comparatorsBaseIndex),
   m_resistors(resistors),
   m_resistorsBaseIndex(resistorsBaseIndex),
   m_wires(wires),
   m_wiresBaseIndex(wiresBaseIndex)
{
   // Capture objects;
   if (m_capacitors) m_capacitors->capture();
   if (m_comparators) m_comparators->capture();
   if (m_resistors) m_resistors->capture();
   if (m_wires) m_wires->capture();
}


AnalogNeuron::~AnalogNeuron()
{
   // Release captured objects;
   if (m_capacitors) m_capacitors->release();
   if (m_comparators) m_comparators->release();
   if (m_resistors) m_resistors->release();
   if (m_wires) m_wires->release();
}


unsigned int AnalogNeuron::getNumInputs() const
{
   return m_inputWires.size();
}


double AnalogNeuron::getPosConstant() const
{
   const unsigned int numInputs = m_inputWires.size();

   // Calculate positive branch resistances products;
   double posProduct = 1.0;
   for (unsigned int i = 0; i < numInputs; ++i)
   {
      double resistance = m_resistors->at(m_resistorsBaseIndex + i);
      if (resistance != 0.0) posProduct *= resistance;
   }

   posProduct = fabs(posProduct);

   // Calculate positive constant;
   double posConstant = 0.0;
   for (unsigned int i = 0; i < numInputs; ++i)
   {
      double resistance = m_resistors->at(m_resistorsBaseIndex + i);
      if (resistance != 0.0) posConstant += posProduct / fabs(resistance);
   }

   if (posConstant != 0.0) posConstant =
      posProduct * m_capacitors->at(m_capacitorsBaseIndex) / posConstant;

   return posConstant;
}


double AnalogNeuron::getNegConstant() const
{
   const unsigned int numInputs = m_inputWires.size();

   // Calculate negative branch resistances products;
   double negProduct = 1.0;
   for (unsigned int i = 0; i < numInputs; ++i)
   {
      double resistance = m_resistors->at(m_resistorsBaseIndex + numInputs + i);
      if (resistance != 0.0) negProduct *= resistance;
   }

   negProduct = fabs(negProduct);

   // Calculate negative constant;
   double negConstant = 0.0;
   for (unsigned int i = 0; i < numInputs; ++i)
   {
      double resistance = m_resistors->at(m_resistorsBaseIndex + numInputs + i);
      if (resistance != 0.0) negConstant += negProduct / fabs(resistance);
   }

   if (negConstant != 0.0) negConstant =
      negProduct * m_capacitors->at(m_capacitorsBaseIndex + 1) / negConstant;

   return negConstant;
}


double AnalogNeuron::leftComputePos() const
{
   return calcPotencial(m_capacitorsBaseIndex, m_resistorsBaseIndex);
}


double AnalogNeuron::leftComputeNeg() const
{
   return -calcPotencial(
      m_capacitorsBaseIndex + 1,
      m_resistorsBaseIndex + m_inputWires.size()
   );
}


void AnalogNeuron::rightCompute(double negPotential, double posPotential)
{
   m_wires->at(m_wiresBaseIndex) = m_comparators->compare(
      m_comparatorsBaseIndex,
      negPotential,
      posPotential
   );
}


void AnalogNeuron::compute()
{
   if (m_comparators)
   {
      // Calculate potentials;
      double posPotential = calcPotencial(
         m_capacitorsBaseIndex,
         m_resistorsBaseIndex
      );
      double negPotential = -calcPotencial(
         m_capacitorsBaseIndex + 1,
         m_resistorsBaseIndex + m_inputWires.size()
      );

      // Transfer positive and negative potentials through the comparator to
      // obtain output voltage;
      m_wires->at(m_wiresBaseIndex) = m_comparators->compare(
         m_comparatorsBaseIndex,
         negPotential,
         posPotential
      );
   }
   else
   {
      // Calculate potential;
      double potential = calcPotencial(
         m_capacitorsBaseIndex,
         m_resistorsBaseIndex
      );

      // Transfer potential;
      m_wires->at(m_wiresBaseIndex) = potential;
   }
}


double AnalogNeuron::calcPotencial(
   unsigned int capacitorsBaseIndex,
   unsigned int resistorsBaseIndex
) const
{
   const unsigned int numInputs = m_inputWires.size();
   double potential = m_wires->at(m_gndWireIndex);
   if (m_capacitors->at(capacitorsBaseIndex) != 0.0)
   {
      // Calculate resistances product;
      double product = 1.0;
      for (unsigned int i = 0; i < numInputs; ++i)
      {
         double resistance = m_resistors->at(resistorsBaseIndex + i);
         if (resistance != 0.0) product *= resistance;
      }

      product = fabs(product);

      // Calculate potential;
      double sum = 0.0;
      for (unsigned int i = 0; i < numInputs; ++i)
      {
         double resistance = m_resistors->at(resistorsBaseIndex + i);
         if (resistance != 0.0)
         {
            double multiplyer = product / resistance;
            potential += multiplyer * m_wires->at(m_inputWires[ i ]);
            sum += fabs(multiplyer);
         }
      }

      // Divide potential over sum;
      if (sum != 0.0) potential /= sum;
   }

   return potential;
}
