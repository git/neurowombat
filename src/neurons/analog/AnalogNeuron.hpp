/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef NEURONS_ANALOG_ANALOGNEURON_HPP
#define NEURONS_ANALOG_ANALOGNEURON_HPP


#include <vector>


#include "components/analog/AnalogCapacitors.hpp"
#include "components/analog/AnalogComparators.hpp"
#include "components/analog/AnalogResistors.hpp"
#include "components/analog/AnalogWires.hpp"
#include "kernel/KernelObject.hpp"


/***************************************************************************
 *   AnalogNeuron class declaration                                        *
 ***************************************************************************/


class AnalogNeuron : public KernelObject
{
   public:
      AnalogNeuron(
         const std::vector<unsigned int> & inputWires,
         unsigned int gndWireIndex,
         unsigned int srcWireIndex,
         AnalogCapacitors * capacitors,
         unsigned int capacitorsBaseIndex,
         AnalogComparators * comparators,
         unsigned int comparatorsBaseIndex,
         AnalogResistors * resistors,
         unsigned int resistorsBaseIndex,
         AnalogWires * wires,
         unsigned int wiresBaseIndex
      );
      virtual ~AnalogNeuron();

      unsigned int getNumInputs() const;
      inline AnalogComparators * comparators() const {return m_comparators;}

      double getPosConstant() const;
      double getNegConstant() const;
      double leftComputePos() const;
      double leftComputeNeg() const;
      void rightCompute(double negPotential, double posPotential);
      void compute();

   private:
      AnalogNeuron(const AnalogNeuron & other);
      AnalogNeuron & operator=(const AnalogNeuron & other);

      double calcPotencial(
         unsigned int capacitorsBaseIndex,
         unsigned int resistorsBaseIndex
      ) const;

      std::vector<unsigned int> m_inputWires;

      unsigned int m_gndWireIndex;
      unsigned int m_srcWireIndex;

      AnalogCapacitors * m_capacitors;
      unsigned int m_capacitorsBaseIndex;

      AnalogComparators * m_comparators;
      unsigned int m_comparatorsBaseIndex;

      AnalogResistors * m_resistors;
      unsigned int m_resistorsBaseIndex;

      AnalogWires * m_wires;
      unsigned int m_wiresBaseIndex;
};


#endif
