/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#include "neurons/digital/DigitalNeuron.hpp"


/***************************************************************************
 *   DigitalNeuron class implementation                                    *
 ***************************************************************************/


DigitalNeuron::DigitalNeuron(
   const std::vector<unsigned int> & inputConnectors,
   DigitalConnectors * connectors,
   unsigned int connectorsBaseIndex,
   MemoryModule * memory,
   unsigned int memoryBaseIndex,
   ProcessingUnit * processingUnit,
   ActivationFunction * activationFunction
) : KernelObject(),
   m_inputConnectors(inputConnectors),
   m_connectors(connectors),
   m_connectorsBaseIndex(connectorsBaseIndex),
   m_memory(memory),
   m_memoryBaseIndex(memoryBaseIndex),
   m_processingUnit(processingUnit),
   m_activationFunction(activationFunction),
   m_processingUnitOut(0.0),
   m_delta(0.0)
{
   // Capture objects;
   if (m_connectors) m_connectors->capture();
   if (m_memory) m_memory->capture();
   if (m_processingUnit) m_processingUnit->capture();
   if (m_activationFunction) m_activationFunction->capture();
}


DigitalNeuron::~DigitalNeuron()
{
   // Release captured objects;
   if (m_connectors) m_connectors->release();
   if (m_memory) m_memory->release();
   if (m_activationFunction) m_activationFunction->release();
   if (m_processingUnit) m_processingUnit->release();
}


unsigned int DigitalNeuron::getInputsCount() const
{
   return m_inputConnectors.size();
}


void DigitalNeuron::setWeight(unsigned int index, double weight)
{
   m_memory->at(m_memoryBaseIndex + index) = weight;
}


double DigitalNeuron::getWeight(unsigned int index) const
{
   return m_memory->at(m_memoryBaseIndex + index);
}


double DigitalNeuron::getOutput() const
{
   return m_connectors->at(m_connectorsBaseIndex);
}


void DigitalNeuron::compute()
{
   // Calculate processor out;
   m_processingUnitOut = m_processingUnit->process(
      m_inputConnectors, m_connectors,
      m_memory, m_memoryBaseIndex
   );

   m_connectors->at(m_connectorsBaseIndex) =
      m_activationFunction->evaluateFunction(m_processingUnitOut);
}


void DigitalNeuron::createDampingBuffers()
{
   if (m_builtInBuffers.empty())
   {
      m_builtInBuffers.resize(m_inputConnectors.size(), 0.0);
   }
}


void DigitalNeuron::snapDelta(double err)
{
   m_delta = err;
   m_delta *= m_activationFunction->evaluateDerivative(m_processingUnitOut);
}


double DigitalNeuron::getWeightedDelta(unsigned int index) const
{
   return m_delta * m_memory->at(m_memoryBaseIndex + index);
}


void DigitalNeuron::modifyWeights(double damping, double speed)
{
   double d = m_delta * speed;
   double dw = 0;

   for (unsigned int i = 0, icount = m_inputConnectors.size(); i < icount; ++i)
   {
      dw = damping * m_builtInBuffers[i] +
         d * m_connectors->at(m_inputConnectors[i]);
      m_builtInBuffers[i] = dw;
      m_memory->at(m_memoryBaseIndex + i) += dw;
   }
}
