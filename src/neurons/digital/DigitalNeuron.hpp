/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef NEURONS_DIGITAL_DIGITALNEURON_HPP
#define NEURONS_DIGITAL_DIGITALNEURON_HPP


#include <vector>


#include "components/digital/DigitalConnectors.hpp"
#include "components/digital/MemoryModule.hpp"
#include "kernel/KernelObject.hpp"
#include "math/ActivationFunction.hpp"
#include "math/ProcessingUnit.hpp"


/***************************************************************************
 *   DigitalNeuron class declaration                                       *
 ***************************************************************************/


class DigitalNeuron : public KernelObject
{
   public:
      DigitalNeuron(
         const std::vector<unsigned int> & inputConnectors,
         DigitalConnectors * connectors,
         unsigned int connectorsBaseIndex,
         MemoryModule * memory,
         unsigned int memoryBaseIndex,
         ProcessingUnit * processingUnit,
         ActivationFunction * activationFunction
      );
      virtual ~DigitalNeuron();

      unsigned int getInputsCount() const;

      void setWeight(unsigned int index, double weight);
      double getWeight(unsigned int index) const;

      double getOutput() const;

      void compute();

      void createDampingBuffers();

      void snapDelta(double err);
      double getWeightedDelta(unsigned int index) const;
      void modifyWeights(double damping, double speed);

   private:
      DigitalNeuron(const DigitalNeuron & other);
      DigitalNeuron & operator=(const DigitalNeuron & other);

      std::vector<unsigned int> m_inputConnectors;

      DigitalConnectors * m_connectors;
      unsigned int m_connectorsBaseIndex;

      MemoryModule * m_memory;
      unsigned int m_memoryBaseIndex;

      std::vector<double> m_builtInBuffers;

      ProcessingUnit * m_processingUnit;
      ActivationFunction * m_activationFunction;

      double m_processingUnitOut;
      double m_delta;
};


#endif
