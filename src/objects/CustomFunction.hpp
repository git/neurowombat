/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef OBJECTS_CUSTOMFUNCTION_HPP
#define OBJECTS_CUSTOMFUNCTION_HPP


#include "kernel/KernelObject.hpp"


/***************************************************************************
 *   CustomFunction abstract class declaration                             *
 ***************************************************************************/


class CustomFunction : public KernelObject
{
   public:
      explicit CustomFunction(int index);
      virtual ~CustomFunction();

      void call() const;
      double call(double x) const;

      inline int functionReference() const {return m_functionReference;}

   private:
      CustomFunction(const CustomFunction & other);
      CustomFunction & operator=(const CustomFunction & other);

      int m_functionReference;
};


#endif
