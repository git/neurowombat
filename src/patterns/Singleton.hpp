/***************************************************************************
 *   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov                  *
 *                                                                         *
 *   This file is part of NeuroWombat.                                     *
 *                                                                         *
 *   NeuroWombat is free software: you can redistribute it and/or modify   *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation, either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   NeuroWombat is distributed in the hope that it will be useful,        *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.  *
 ***************************************************************************/


#ifndef PATTERNS_SINGLETON_HPP
#define PATTERNS_SINGLETON_HPP


#include <cstddef>


/***************************************************************************
 *   Singleton class declaration                                           *
 ***************************************************************************/


template <class T>
class Singleton
{
   public:
      static T * instance();
      void freeInstance();

   protected:
      Singleton();
      virtual ~Singleton();

   private:
      enum CONSTANTS
      {
         MAX_REFERENCE_COUNT = 0xFFFFFFFF
      };

      static T * m_self;
      static size_t m_referenceCount;
};


/***************************************************************************
 *   Singleton class implementation                                        *
 ***************************************************************************/

template <class T>
T * Singleton<T>::instance()
{
   if (m_referenceCount >= Singleton<T>::MAX_REFERENCE_COUNT) return NULL;

   if (!m_self)
   {
      m_self = new T();
   }

   ++m_referenceCount;
   return m_self;
}


template <class T>
void Singleton<T>::freeInstance()
{
   --m_referenceCount;

   if (!m_referenceCount)
   {
      delete this;
   }
}


template <class T>
Singleton<T>::Singleton()
{
}


template <class T>
Singleton<T>::~Singleton()
{
   m_self = NULL;
}


template <class T>
T * Singleton<T>::m_self = NULL;


template <class T>
size_t Singleton<T>::m_referenceCount = 0;


#endif
