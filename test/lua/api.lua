--   Copyright (C) 2009, 2010, 2014, 2018 Andrey Timashov
--
--   This file is part of NeuroWombat.
--
--   NeuroWombat is free software: you can redistribute it and/or modify
--   it under the terms of the GNU General Public License as published by
--   the Free Software Foundation, either version 3 of the License, or
--   (at your option) any later version.
--
--   NeuroWombat is distributed in the hope that it will be useful,
--   but WITHOUT ANY WARRANTY; without even the implied warranty of
--   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--   GNU General Public License for more details.
--
--   You should have received a copy of the GNU General Public License
--   along with NeuroWombat.  If not, see <http://www.gnu.org/licenses/>.


function arrayToString(arr)
   local s = "{";
   for i = 1, #arr do
      if i > 1 then
         s = s .. ", " .. arr[i];
      else
         s = s .. arr[i];
      end
   end
   return s .. "}";
end


function compareFloatArrays(lhs, rhs, err)
   if #lhs ~= #rhs then
      print(arrayToString(lhs) .. " ~= " .. arrayToString(rhs));
      return false;
   end

   for i = 1, #lhs do
      if math.abs(lhs[i] - rhs[i]) > err then
         print(arrayToString(lhs) .. " ~= " .. arrayToString(rhs));
         return false;
      end
   end

   return true;
end


function compareFloat(lhs, rhs, err)
   if math.abs(lhs - rhs) > err then
      print(lhs .. " ~= " .. rhs);
      return false;
   end

   return true;
end


function testCloseId()
   -- A simple case;
   local id = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);
   closeId(id); -- Activation function will be released and then deleted;

   -- Dependant objects case;
   -- Create components of the future neuron;
   local connectorsId = createAbstractConnectors(3);
   local weightsId = createAbstractWeights(2);
   local procUnitId = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFuncId = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);

   -- Create the neuron;
   local neuronId = createAbstractNeuron(2, {0, 1}, connectorsId, 2, weightsId, 0, procUnitId, actFuncId);

   -- Release objects in arbitrary order;
   closeId(weightsId);    -- Weights object will be released but not deleted;
   closeId(connectorsId); -- Connectors object will be released but not deleted;
   closeId(procUnitId);   -- Processing unit will be released but not deleted;
   closeId(neuronId);     -- Neuron, weights, connectors and processing unit will be deleted;
   closeId(actFuncId);    -- Activation function will be released and then deleted;
end


function testCreateAbstractConnectors()
   local connectors = createAbstractConnectors(5);
   assert(connectors > 0);
   closeId(connectors);
end


function testGetSignals()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(5);
   setSignals(connectors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getSignals(connectors, 3, 0), {}, delta));
   assert(compareFloatArrays(getSignals(connectors, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getSignals(connectors, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getSignals(connectors, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getSignals(connectors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));
   closeId(connectors);
end


function testSetSignals()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(5);

   setSignals(connectors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getSignals(connectors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setSignals(connectors, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getSignals(connectors, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setSignals(connectors, 4, {9.0});
   assert(compareFloatArrays(getSignals(connectors, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(connectors);
end


function testCreateAbstractWeights()
   local weights = createAbstractWeights(5);
   assert(weights > 0);
   closeId(weights);
end


function testGetAbstractWeights()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(3);
   local weights = createAbstractWeights(2);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);

   -- Get all the weights from the abstract neuron with external weights;
   local neuron1 = createAbstractNeuron(2, {0, 1}, connectors, 2, weights, 0, procUnit, actFunc);
   setAbstractWeights(weights, 0, {2.5, 7.0});
   assert(compareFloatArrays(getAbstractWeights(neuron1), {2.5, 7.0}, delta));

   -- Get all the weights from the abstract neuron with built-in weights;
   local neuron2 = createAbstractNeuron(2, {0, 1}, connectors, 2, 0, 0, procUnit, actFunc);
   setAbstractWeights(neuron2, {3.5, 8.0});
   assert(compareFloatArrays(getAbstractWeights(neuron2), {3.5, 8.0}, delta));

   closeId(neuron2);
   closeId(neuron1);
   closeId(actFunc);
   closeId(procUnit);
   closeId(weights);
   closeId(connectors);

   -- Read the weights values from the abstract weights object;
   weights = createAbstractWeights(5);
   setAbstractWeights(weights, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getAbstractWeights(weights, 3, 0), {}, delta));
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getAbstractWeights(weights, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getAbstractWeights(weights, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   closeId(weights);
end


function testSetAbstractWeights()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(3);
   local weights = createAbstractWeights(2);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);

   -- Set the weights values to the abstract neuron with external weights;
   local neuron1 = createAbstractNeuron(2, {0, 1}, connectors, 2, weights, 0, procUnit, actFunc);
   setAbstractWeights(neuron1, {1.0, 5.5});
   assert(compareFloatArrays(getAbstractWeights(neuron1), {1.0, 5.5}, delta));
   setAbstractWeights(weights, 1, {6.0});
   assert(compareFloatArrays(getAbstractWeights(neuron1), {1.0, 6.0}, delta));

   -- Set the weights values to the abstract neuron with built-in weights;
   local neuron2 = createAbstractNeuron(2, {0, 1}, connectors, 2, 0, 0, procUnit, actFunc);
   setAbstractWeights(neuron2, {7.0, 3.0});
   assert(compareFloatArrays(getAbstractWeights(neuron2), {7.0, 3.0}, delta));

   closeId(neuron2);
   closeId(neuron1);
   closeId(actFunc);
   closeId(procUnit);
   closeId(weights);
   closeId(connectors);

   -- Write the weights values to the abstract weights object;
   weights = createAbstractWeights(5);

   setAbstractWeights(weights, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setAbstractWeights(weights, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setAbstractWeights(weights, 4, {9.0});
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(weights);
end


function testCreateAbstractNeuron()
   -- Create a neuron with built-in weights;
   local connectors = createAbstractConnectors(3);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.SIGMOID, 1.0);
   local neuron = createAbstractNeuron(2, {0, 1}, connectors, 2, 0, 0, procUnit, actFunc);
   assert(neuron > 0);
   closeId(neuron);
   closeId(actFunc);
   closeId(procUnit);
   closeId(connectors);

   -- Create a pair of neurons with external weights;
   connectors = createAbstractConnectors(6);
   weights = createAbstractWeights(5);
   procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   actFunc = createActFunc(ACT_FUNC.SIGMOID, 1.0);
   local neuron1 = createAbstractNeuron(3, {0, 2, 1}, connectors, 3, weights, 0, procUnit, actFunc);
   assert(neuron1 > 0);
   local neuron2 = createAbstractNeuron(2, {0, 4}, connectors, 5, weights, 3, procUnit, actFunc);
   assert(neuron2 > 0);
   setSignals(connectors, 0, {1.0}); -- Connector 0 represents a bias input;
   closeId(neuron2);
   closeId(neuron1);
   closeId(actFunc);
   closeId(procUnit);
   closeId(weights);
   closeId(connectors);
end


function testComputeAbstractNeurons()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(6);
   local weights = createAbstractWeights(3);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);
   setSignals(connectors, 0, {1.0}); -- Connector 0 represents a bias input;
   assert(compareFloatArrays(getSignals(connectors, 0, 1), {1.0}, delta));

   local neuron1 = createAbstractNeuron(3, {0, 2, 1}, connectors, 3, weights, 0, procUnit, actFunc);
   setAbstractWeights(neuron1, {7.0, 10.0, 5.0}); -- Set neuron1 weights;
   setSignals(connectors, 1, {0.2, 0.1}); -- Set neuron1 input signals;

   local neuron2 = createAbstractNeuron(2, {0, 4}, connectors, 5, 0, 0, procUnit, actFunc);
   setAbstractWeights(neuron2, {4.0, 2.0}); -- Set neuron2 weights;
   setSignals(connectors, 4, {0.5}); -- Set neuron2 input signals;

   computeAbstractNeurons({neuron1, neuron2}, 1);
   assert(compareFloatArrays(getSignals(connectors, 3, 1), {9.0}, delta));
   assert(compareFloatArrays(getSignals(connectors, 5, 1), {5.0}, delta));

   closeId(neuron2);
   closeId(neuron1);
   closeId(actFunc);
   closeId(procUnit);
   closeId(weights);
   closeId(connectors);
end


function testComputeAbstractNeuronsC()
   -- TODO;
end


function testTrainBPAbstractNeurons()
   local delta = 0.1;
   local connectors = createAbstractConnectors(5);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.SIGMOID, 1.0);

   local neurons = {
      createAbstractNeuron(2, {0, 1}, connectors, 2, 0, 0, procUnit, actFunc),
      createAbstractNeuron(2, {0, 1}, connectors, 3, 0, 0, procUnit, actFunc),
      createAbstractNeuron(2, {2, 3}, connectors, 4, 0, 0, procUnit, actFunc)
   };
   for i = 1, #neurons do
      setAbstractWeights(neurons[i], {0.2, 0.8});
   end

   local targets = { -- XOR function;
      {x = 0.0, y = 0.0, f = 0.0},
      {x = 0.0, y = 1.0, f = 1.0},
      {x = 1.0, y = 0.0, f = 1.0},
      {x = 1.0, y = 1.0, f = 0.0}
   };

   local success = false;
   for i = 1, 100000 do
      local maxErr = 0.0;
      for j = 1, #targets do
         setSignals(connectors, 0, {targets[j].x, targets[j].y});
         trainBPAbstractNeurons(neurons, {2, 1}, {targets[j].f}, 0.5, 0.25);
         computeAbstractNeurons(neurons, 1);
         local err = math.abs(targets[j].f - getSignals(connectors, 4, 1)[1]);
         maxErr = math.max(maxErr, err);
      end

      if maxErr < 0.075 then
         success = true;
         break;
      end
   end

   assert(success);

   setSignals(connectors, 0, {0.01, 0.01});
   computeAbstractNeurons(neurons, 1);
   assert(compareFloatArrays(getSignals(connectors, 4, 1), {0.0}, delta));

   setSignals(connectors, 0, {0.01, 0.99});
   computeAbstractNeurons(neurons, 1);
   assert(compareFloatArrays(getSignals(connectors, 4, 1), {1.0}, delta));

   setSignals(connectors, 0, {0.99, 0.01});
   computeAbstractNeurons(neurons, 1);
   assert(compareFloatArrays(getSignals(connectors, 4, 1), {1.0}, delta));

   setSignals(connectors, 0, {0.99, 0.99});
   computeAbstractNeurons(neurons, 1);
   assert(compareFloatArrays(getSignals(connectors, 4, 1), {0.0}, delta));

   for i = 1, #neurons do
      closeId(neurons[i]);
   end
   closeId(actFunc);
   closeId(procUnit);
   closeId(connectors);
end


function testCreateAnalogCapacitors()
   local capacitors = createAnalogCapacitors(5);
   assert(capacitors > 0);
   closeId(capacitors);
end


function testGetAnalogCapacitances()
   local delta = 0.0001;
   local capacitors = createAnalogCapacitors(5);
   setAnalogCapacitances(capacitors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 3, 0), {}, delta));
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));
   closeId(capacitors);
end


function testSetAnalogCapacitances()
   local delta = 0.0001;
   local capacitors = createAnalogCapacitors(5);

   setAnalogCapacitances(capacitors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setAnalogCapacitances(capacitors, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setAnalogCapacitances(capacitors, 4, {9.0});
   assert(compareFloatArrays(getAnalogCapacitances(capacitors, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(capacitors);
end


function testCreateAnalogComparators()
   local comparators = createAnalogComparators(5);
   assert(comparators > 0);
   closeId(comparators);
end


function testCreateAnalogResistors()
   local resistors = createAnalogResistors(5);
   assert(resistors > 0);
   closeId(resistors);
end


function testSetupAnalogResistors()
   local delta = 0.0001;
   local resistors = createAnalogResistors(10);

   setupAnalogResistors(resistors, 0, 5, {0.0, 2.0, 5.5, 1.0, 7.0}, 1);
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 5), {0.0, 2.12716, 0.77351, 4.25432, 0.60776}, delta));

   setupAnalogResistors(resistors, 0, 5, {0.0, 2.0, 0.0, 0.0, 0.0}, 1);
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 5), {0.0, 2.0, 0.0, 0.0, 0.0}, delta));

   setupAnalogResistors(resistors, 0, 5, {0.0, 2.0, 5.5, 1.0, 7.0}, 2);
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 10), {0.0, 2.12716, 0.77351, 4.25432, 0.60776, 0.0, 2.12716, 0.77351, 4.25432, 0.60776}, delta));

   closeId(resistors);
end


function testGetAnalogResistances()
   local delta = 0.0001;
   local resistors = createAnalogResistors(5);
   setAnalogResistances(resistors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getAnalogResistances(resistors, 3, 0), {}, delta));
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getAnalogResistances(resistors, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getAnalogResistances(resistors, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));
   closeId(resistors);
end


function testSetAnalogResistances()
   local delta = 0.0001;
   local resistors = createAnalogResistors(5);

   setAnalogResistances(resistors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setAnalogResistances(resistors, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setAnalogResistances(resistors, 4, {9.0});
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(resistors);
end


function testCreateAnalogWires()
   local wires = createAnalogWires(5);
   assert(wires > 0);
   closeId(wires);
end


function testGetPotentials()
   local delta = 0.0001;
   local wires = createAnalogWires(5);
   setPotentials(wires, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getPotentials(wires, 3, 0), {}, delta));
   assert(compareFloatArrays(getPotentials(wires, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getPotentials(wires, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getPotentials(wires, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getPotentials(wires, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));
   closeId(wires);
end


function testSetPotentials()
   local delta = 0.0001;
   local wires = createAnalogWires(5);

   setPotentials(wires, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getPotentials(wires, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setPotentials(wires, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getPotentials(wires, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setPotentials(wires, 4, {9.0});
   assert(compareFloatArrays(getPotentials(wires, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(wires);
end


function testCreateAnalogNeuron()
   -- Create a neuron with linear activation function;
   local wires = createAnalogWires(5);
   local capacitors = createAnalogCapacitors(1);
   local resistors = createAnalogResistors(2);
   local neuron = createAnalogNeuron(2, {2, 3}, 0, 1, capacitors, 0, 0, 0, resistors, 0, wires, 4);
   assert(neuron > 0);
   setPotentials(wires, 0, {0.0, 1.0}); -- Wire 0 represents a ground, wire 1 represents a source;
   closeId(neuron);
   closeId(resistors);
   closeId(capacitors);
   closeId(wires);

   -- Create a pair of neurons with limit activation function;
   wires = createAnalogWires(7);
   capacitors = createAnalogCapacitors(4);
   local comparators = createAnalogComparators(2);
   resistors = createAnalogResistors(10);
   local neuron1 = createAnalogNeuron(3, {1, 3, 2}, 0, 1, capacitors, 0, comparators, 0, resistors, 0, wires, 4);
   assert(neuron1 > 0);
   local neuron2 = createAnalogNeuron(2, {1, 5}, 0, 1, capacitors, 2, comparators, 1, resistors, 6, wires, 6);
   assert(neuron2 > 0);
   setPotentials(wires, 0, {0.0, 1.0}); -- Wire 0 represents a ground, wire 1 represents a source;
   closeId(neuron2);
   closeId(neuron1);
   closeId(resistors);
   closeId(comparators);
   closeId(capacitors);
   closeId(wires);
end


function testComputeAnalogNeurons()
   local delta = 0.0001;
   local wires = createAnalogWires(7);
   local capacitors = createAnalogCapacitors(2);
   local resistors = createAnalogResistors(5);
   setPotentials(wires, 0, {0.0, 1.0}); -- Wire 0 represents a ground, wire 1 represents a source;
   assert(compareFloatArrays(getPotentials(wires, 0, 2), {0.0, 1.0}, delta));

   setAnalogCapacitances(capacitors, 0, {1.0, 1.0});

   local neuron1 = createAnalogNeuron(3, {1, 3, 2}, 0, 1, capacitors, 0, 0, 0, resistors, 0, wires, 4);
   setupAnalogResistors(resistors, 0, 3, {7.0, 10.0, 5.0}, 1); -- Set neuron1 weights;
   setPotentials(wires, 2, {0.2, 0.1}); -- Set neuron1 input signals;

   local neuron2 = createAnalogNeuron(2, {1, 5}, 0, 1, capacitors, 1, 0, 0, resistors, 3, wires, 6);
   setupAnalogResistors(resistors, 3, 2, {4.0, 2.0}, 1); -- Set neuron2 weights;
   setPotentials(wires, 5, {0.5}); -- Set neuron2 input signals;

   computeAnalogNeurons({neuron1, neuron2}, 1);
   assert(compareFloatArrays(getPotentials(wires, 4, 1), {0.40909}, delta));
   assert(compareFloatArrays(getPotentials(wires, 6, 1), {0.83333}, delta));

   closeId(neuron2);
   closeId(neuron1);
   closeId(resistors);
   closeId(capacitors);
   closeId(wires);
end


function testComputeAnalogLimNeuronsC()
   -- TODO;
end


function testCreateDigitalConnectors()
   local connectors = createDigitalConnectors(5);
   assert(connectors > 0);
   closeId(connectors);
end


function testGetValues()
   local delta = 0.0001;
   local connectors = createDigitalConnectors(5);
   setValues(connectors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getValues(connectors, 3, 0), {}, delta));
   assert(compareFloatArrays(getValues(connectors, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getValues(connectors, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getValues(connectors, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getValues(connectors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));
   closeId(connectors);
end


function testSetValues()
   local delta = 0.0001;
   local connectors = createDigitalConnectors(5);

   setValues(connectors, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getValues(connectors, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setValues(connectors, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getValues(connectors, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setValues(connectors, 4, {9.0});
   assert(compareFloatArrays(getValues(connectors, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(connectors);
end


function testCreateMemoryModule()
   local memory = createMemoryModule(5);
   assert(memory > 0);
   closeId(memory);
end


function testGetDigitalWeights()
   local delta = 0.0001;
   local connectors = createDigitalConnectors(3);
   local memory = createMemoryModule(2);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);

   -- Get all the weights from the digital neuron;
   local neuron = createDigitalNeuron(2, {0, 1}, connectors, 2, memory, 0, procUnit, actFunc);
   setDigitalWeights(memory, 0, {2.5, 7.0});
   assert(compareFloatArrays(getDigitalWeights(neuron), {2.5, 7.0}, delta));

   closeId(neuron);
   closeId(actFunc);
   closeId(procUnit);
   closeId(memory);
   closeId(connectors);

   -- Read the words from the memory module;
   memory = createMemoryModule(5);
   setDigitalWeights(memory, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getDigitalWeights(memory, 3, 0), {}, delta));
   assert(compareFloatArrays(getDigitalWeights(memory, 0, 2), {0.0, 2.0}, delta));
   assert(compareFloatArrays(getDigitalWeights(memory, 2, 1), {5.5}, delta));
   assert(compareFloatArrays(getDigitalWeights(memory, 3, 2), {1.0, 7.0}, delta));
   assert(compareFloatArrays(getDigitalWeights(memory, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   closeId(memory);
end


function testSetDigitalWeights()
   local delta = 0.0001;
   local connectors = createDigitalConnectors(3);
   local memory = createMemoryModule(2);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);

   -- Set the weights values to the digital neuron;
   local neuron = createDigitalNeuron(2, {0, 1}, connectors, 2, memory, 0, procUnit, actFunc);
   setDigitalWeights(neuron, {1.0, 5.5});
   assert(compareFloatArrays(getDigitalWeights(neuron), {1.0, 5.5}, delta));
   setDigitalWeights(memory, 1, {6.0});
   assert(compareFloatArrays(getDigitalWeights(neuron), {1.0, 6.0}, delta));

   closeId(neuron);
   closeId(actFunc);
   closeId(procUnit);
   closeId(memory);
   closeId(connectors);

   -- Write the words to the memory module;
   memory = createMemoryModule(5);

   setDigitalWeights(memory, 0, {0.0, 2.0, 5.5, 1.0, 7.0});
   assert(compareFloatArrays(getDigitalWeights(memory, 0, 5), {0.0, 2.0, 5.5, 1.0, 7.0}, delta));

   setDigitalWeights(memory, 1, {8.0, 7.5, 3.0});
   assert(compareFloatArrays(getDigitalWeights(memory, 0, 5), {0.0, 8.0, 7.5, 3.0, 7.0}, delta));

   setDigitalWeights(memory, 4, {9.0});
   assert(compareFloatArrays(getDigitalWeights(memory, 0, 5), {0.0, 8.0, 7.5, 3.0, 9.0}, delta));

   closeId(memory);
end


function testCreateDigitalNeuron()
   local connectors = createDigitalConnectors(6);
   local memory = createMemoryModule(5);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.SIGMOID, 1.0);
   local neuron1 = createDigitalNeuron(3, {0, 2, 1}, connectors, 3, memory, 0, procUnit, actFunc);
   assert(neuron1 > 0);
   local neuron2 = createDigitalNeuron(2, {0, 4}, connectors, 5, memory, 3, procUnit, actFunc);
   assert(neuron2 > 0);
   setValues(connectors, 0, {1.0}); -- Connector 0 represents a bias input;
   closeId(neuron2);
   closeId(neuron1);
   closeId(actFunc);
   closeId(procUnit);
   closeId(memory);
   closeId(connectors);
end


function testComputeDigitalNeurons()
   local delta = 0.0001;
   local connectors = createDigitalConnectors(6);
   local memory = createMemoryModule(5);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);
   setValues(connectors, 0, {1.0}); -- Connector 0 represents a bias input;
   assert(compareFloatArrays(getValues(connectors, 0, 1), {1.0}, delta));

   local neuron1 = createDigitalNeuron(3, {0, 2, 1}, connectors, 3, memory, 0, procUnit, actFunc);
   setDigitalWeights(neuron1, {7.0, 10.0, 5.0}); -- Set neuron1 weights;
   setValues(connectors, 1, {0.2, 0.1}); -- Set neuron1 input values;

   local neuron2 = createDigitalNeuron(2, {0, 4}, connectors, 5, memory, 3, procUnit, actFunc);
   setDigitalWeights(neuron2, {4.0, 2.0}); -- Set neuron2 weights;
   setValues(connectors, 4, {0.5}); -- Set neuron2 input values;

   computeDigitalNeurons({neuron1, neuron2}, 1);
   assert(compareFloatArrays(getValues(connectors, 3, 1), {9.0}, delta));
   assert(compareFloatArrays(getValues(connectors, 5, 1), {5.0}, delta));

   closeId(neuron2);
   closeId(neuron1);
   closeId(actFunc);
   closeId(procUnit);
   closeId(memory);
   closeId(connectors);
end


function testTrainBPDigitalNeurons()
   local delta = 0.1;
   local connectors = createDigitalConnectors(5);
   local memory = createMemoryModule(6);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   local actFunc = createActFunc(ACT_FUNC.SIGMOID, 1.0);

   local neurons = {
      createDigitalNeuron(2, {0, 1}, connectors, 2, memory, 0, procUnit, actFunc),
      createDigitalNeuron(2, {0, 1}, connectors, 3, memory, 2, procUnit, actFunc),
      createDigitalNeuron(2, {2, 3}, connectors, 4, memory, 4, procUnit, actFunc)
   };
   for i = 1, #neurons do
      setDigitalWeights(neurons[i], {0.2, 0.8});
   end

   local targets = { -- XOR function;
      {x = 0.0, y = 0.0, f = 0.0},
      {x = 0.0, y = 1.0, f = 1.0},
      {x = 1.0, y = 0.0, f = 1.0},
      {x = 1.0, y = 1.0, f = 0.0}
   };

   local success = false;
   for i = 1, 100000 do
      local maxErr = 0.0;
      for j = 1, #targets do
         setValues(connectors, 0, {targets[j].x, targets[j].y});
         trainBPDigitalNeurons(neurons, {2, 1}, {targets[j].f}, 0.5, 0.25);
         computeDigitalNeurons(neurons, 1);
         local err = math.abs(targets[j].f - getValues(connectors, 4, 1)[1]);
         maxErr = math.max(maxErr, err);
      end

      if maxErr < 0.075 then
         success = true;
         break;
      end
   end

   assert(success);

   setValues(connectors, 0, {0.01, 0.01});
   computeDigitalNeurons(neurons, 1);
   assert(compareFloatArrays(getValues(connectors, 4, 1), {0.0}, delta));

   setValues(connectors, 0, {0.01, 0.99});
   computeDigitalNeurons(neurons, 1);
   assert(compareFloatArrays(getValues(connectors, 4, 1), {1.0}, delta));

   setValues(connectors, 0, {0.99, 0.01});
   computeDigitalNeurons(neurons, 1);
   assert(compareFloatArrays(getValues(connectors, 4, 1), {1.0}, delta));

   setValues(connectors, 0, {0.99, 0.99});
   computeDigitalNeurons(neurons, 1);
   assert(compareFloatArrays(getValues(connectors, 4, 1), {0.0}, delta));

   for i = 1, #neurons do
      closeId(neurons[i]);
   end
   closeId(actFunc);
   closeId(procUnit);
   closeId(memory);
   closeId(connectors);
end


function testCreateActFunc()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(2);
   local weights = createAbstractWeights(1);
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   setSignals(connectors, 0, {0.7});
   setAbstractWeights(weights, 0, {1.0});

   -- Test custom activation function;
   function customEval(x)
      return x * x;
   end
   function customDirEval(x)
      return 2 * x;
   end
   local actFunc = createActFunc(ACT_FUNC.CUSTOM, customEval, customDirEval);
   assert(actFunc > 0);
   local neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {0.49}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test gaussian activation function;
   actFunc = createActFunc(ACT_FUNC.GAUSSIAN, 2.0);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {0.37531}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test step activation function;
   actFunc = createActFunc(ACT_FUNC.LIM, 2.0, -1.0, 1.0);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {-1.0}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test linear activation function;
   actFunc = createActFunc(ACT_FUNC.LINEAR, 2.0, -3.0);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {-1.6}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test threshold activation function;
   actFunc = createActFunc(ACT_FUNC.LIMLINEAR, 2.0, -3.0, -5.0, 5.0);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {-1.6}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test positive linear activation function;
   actFunc = createActFunc(ACT_FUNC.POSLINEAR, 2.0, -3.0);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {0.0}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test sigmoid activation function;
   actFunc = createActFunc(ACT_FUNC.SIGMOID, 2.0);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {0.80218}, delta));
   closeId(neuron);
   closeId(actFunc);

   -- Test hyperbolic tangent sigmoid activation function;
   actFunc = createActFunc(ACT_FUNC.THSIGMOID);
   assert(actFunc > 0);
   neuron = createAbstractNeuron(1, {0}, connectors, 1, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 1, 1), {0.60437}, delta));
   closeId(neuron);
   closeId(actFunc);

   closeId(procUnit);
   closeId(weights);
   closeId(connectors);
end

function testCreateProcUnit()
   local delta = 0.0001;
   local connectors = createAbstractConnectors(4);
   local weights = createAbstractWeights(3);
   local actFunc = createActFunc(ACT_FUNC.LINEAR, 1.0, 0.0);
   setSignals(connectors, 0, {0.7, -0.5, 0.3});
   setAbstractWeights(weights, 0, {2.0, 3.0, 1.5});

   -- Test custom processing unit;
   function customProcessing(x, w)
      local net = 0.0;
      local i;
      for i = 1, #x do net = net + (x[i] + w[i]); end
      return net;
   end
   local procUnit = createProcUnit(PROC_UNIT.CUSTOM, customProcessing);
   assert(procUnit > 0);
   local neuron = createAbstractNeuron(3, {0, 1, 2}, connectors, 3, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 3, 1), {7.0}, delta));
   closeId(neuron);
   closeId(procUnit);

   -- Test radial-basis processing unit;
   -- TODO: Test all the values of coefficient usage enumeration;
   local procUnit = createProcUnit(PROC_UNIT.RADIAL_BASIS, COEFF_USAGE.NOP);
   assert(procUnit > 0);
   local neuron = createAbstractNeuron(3, {0, 1, 2}, connectors, 3, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 3, 1), {3.92173}, delta));
   closeId(neuron);
   closeId(procUnit);

   -- Test scalar processing unit;
   local procUnit = createProcUnit(PROC_UNIT.SCALAR);
   assert(procUnit > 0);
   local neuron = createAbstractNeuron(3, {0, 1, 2}, connectors, 3, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 3, 1), {0.7}, delta));
   closeId(neuron);
   closeId(procUnit);

   -- Test weighted sum processing unit;
   local procUnit = createProcUnit(PROC_UNIT.WEIGHTED_SUM);
   assert(procUnit > 0);
   local neuron = createAbstractNeuron(3, {0, 1, 2}, connectors, 3, weights, 0, procUnit, actFunc);
   computeAbstractNeurons({neuron}, 1);
   assert(compareFloatArrays(getSignals(connectors, 3, 1), {0.35}, delta));
   closeId(neuron);
   closeId(procUnit);

   closeId(actFunc);
   closeId(weights);
   closeId(connectors);
end


function testCreateDistribution()
   local distr = createDistribution(DISTR.EXP, 3.0);
   assert(distr > 0);
   closeId(distr);

   distr = createDistribution(DISTR.WEIBULL, 3.0, 2.0);
   assert(distr > 0);
   closeId(distr);

   function uniformQuantileFunction(p)
      return (5.0 - 3.0) * p + 3.0;
   end
   distr = createDistribution(DISTR.CUSTOM, uniformQuantileFunction);
   assert(distr > 0);
   closeId(distr);
end


function testCalcMeanCI()
   local delta = 0.0001;

   -- Let the data set to be as follows;
   local x = {1.35, 1.34, 1.37, 1.36, 1.35, 1.40, 1.35, 1.37, 1.32, 1.34};

   -- It's statistics can be estimated like that;
   local mean = 0.0;
   local meansqr = 0.0;
   for i = 1, #x do
      mean = mean + x[i];
      meansqr = meansqr + x[i] * x[i];
   end

   mean = mean / #x; -- 1.355
   meansqr = meansqr / #x; -- 1.83645

   -- To get the desired confidence interval simply call the function;
   assert(compareFloat(calcMeanCI(mean, meansqr, #x, 0.95), 0.01347, delta));
end


function testCalcACProbabilityCI()
   local delta = 0.01;

   -- Let the random variable x has the following values;
   local x = {1.35, 1.34, 1.37, 1.36, 1.35, 1.40, 1.35, 1.37, 1.32, 1.34};

   -- The probability of x > 1.34 can be estimated like that;
   local p = 0.0;
   for i = 1, #x do
      if x[i] > 1.34 then p = p + 1.0 end
   end

   p = p / #x; -- 0.7

   -- To get the desired confidence interval simply call the function;
   local pMin, pMax = calcACProbabilityCI(p, #x, 0.05);
   assert(compareFloat(pMin, 0.392, delta));
   assert(compareFloat(pMax, 0.897, delta));
end


function testCreateInterruptManager()
   local distr = createDistribution(DISTR.EXP, 3.0);

   -- Create abstract weights manager;
   local abstractWeights = createAbstractWeights(5);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   assert(mng > 0);
   closeId(mng);
   closeId(abstractWeights);

   -- Create analog capacitors manager;
   local analogCapacitors = createAnalogCapacitors(2);
   mng = createInterruptManager(analogCapacitors, distr, nil);
   assert(mng > 0);
   closeId(mng);
   closeId(analogCapacitors);

   -- Create analog resistors manager;
   local analogResistors = createAnalogResistors(3);
   mng = createInterruptManager(analogResistors, distr, nil);
   assert(mng > 0);
   closeId(mng);
   closeId(analogResistors);

   -- Create memory module manager;
   local memoryModule = createMemoryModule(5);
   local digitalWeightBackup = getDigitalWeights(memoryModule, 0, 5);
   function fixMemoryModule()
      setDigitalWeights(memoryModule, 0, digitalWeightBackup);
   end
   mng = createInterruptManager(memoryModule, distr, fixMemoryModule);
   assert(mng > 0);
   closeId(mng);
   closeId(memoryModule);

   closeId(distr);
end


function testCreateSimulationEngine()
   local engine = createSimulationEngine();
   assert(engine > 0);
   closeId(engine);
end


function testAppendInterruptManager()
   local distr = createDistribution(DISTR.EXP, 3.0);
   local abstractWeights = createAbstractWeights(5);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   local engine = createSimulationEngine();
   appendInterruptManager(engine, mng);
   closeId(engine);
   closeId(mng);
   closeId(abstractWeights);
   closeId(distr);
end


function testGetIntSourcesCount()
   local distr = createDistribution(DISTR.EXP, 3.0);
   local abstractWeights = createAbstractWeights(5);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   assert(getIntSourcesCount(mng) == 5);
   closeId(mng);
   closeId(abstractWeights);
   closeId(distr);
end


function testGetInterruptsCount()
   local distr = createDistribution(DISTR.EXP, 3.0);
   local abstractWeights = createAbstractWeights(5);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   local engine = createSimulationEngine();
   appendInterruptManager(engine, mng);
   assert(getInterruptsCount(mng) == 0);
   stepOverEngine(engine);
   assert(getInterruptsCount(mng) == 1);
   stepOverEngine(engine);
   assert(getInterruptsCount(mng) == 2);
   stepOverEngine(engine);
   assert(getInterruptsCount(mng) == 3);
   stepOverEngine(engine);
   assert(getInterruptsCount(mng) == 4);
   stepOverEngine(engine);
   assert(getInterruptsCount(mng) == 5);
   stepOverEngine(engine);
   assert(getInterruptsCount(mng) == 5);
   closeId(engine);
   closeId(mng);
   closeId(abstractWeights);
   closeId(distr);
end


function testSimulateInterrupt()
   local delta = 0.0001;
   local distr = createDistribution(DISTR.EXP, 3.0);
   local weights = createAbstractWeights(3);
   setAbstractWeights(weights, 0, {1.0, 5.0, 7.0});
   local mng = createInterruptManager(weights, distr, nil);
   simulateInterrupt(mng, 0);
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 3), {0.0, 5.0, 7.0}, delta));
   simulateInterrupt(mng, 2);
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 3), {0.0, 5.0, 0.0}, delta));
   simulateInterrupt(mng, 1);
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 3), {0.0, 0.0, 0.0}, delta));
   closeId(mng);
   closeId(weights);
   closeId(distr);
end


function testRestartEngine()
   local delta = 0.0001;
   local distr = createDistribution(DISTR.EXP, 3.0);

   local weights = createAbstractWeights(3);
   setAbstractWeights(weights, 0, {1.0, 5.0, 7.0});
   local weightsMng = createInterruptManager(weights, distr, nil);

   local resistors = createAnalogResistors(2);
   setAnalogResistances(resistors, 0, {8.0, 2.0});
   function fixAnalogResistors()
      setAnalogResistances(resistors, 0, {-1.0, -3.0});
   end
   local resistorsMng = createInterruptManager(resistors, distr, fixAnalogResistors);

   local engine = createSimulationEngine();
   appendInterruptManager(engine, weightsMng);
   appendInterruptManager(engine, resistorsMng);

   stepOverEngine(engine);
   stepOverEngine(engine);
   stepOverEngine(engine);
   stepOverEngine(engine);
   stepOverEngine(engine);
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 3), {0.0, 0.0, 0.0}, delta));
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 2), {0.0, 0.0}, delta));
   restartEngine(engine);
   assert(compareFloatArrays(getAbstractWeights(weights, 0, 3), {1.0, 5.0, 7.0}, delta));
   assert(compareFloatArrays(getAnalogResistances(resistors, 0, 2), {-1.0, -3.0}, delta));

   closeId(engine);
   closeId(resistorsMng);
   closeId(resistors);
   closeId(weightsMng);
   closeId(weights);
   closeId(distr);
end


function testStepOverEngine()
   local distr = createDistribution(DISTR.EXP, 3.0);
   local abstractWeights = createAbstractWeights(3);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   local engine = createSimulationEngine();
   appendInterruptManager(engine, mng);
   assert(stepOverEngine(engine));
   assert(stepOverEngine(engine));
   assert(stepOverEngine(engine));
   assert(not stepOverEngine(engine));
   assert(not stepOverEngine(engine));
   restartEngine(engine);
   assert(stepOverEngine(engine));
   assert(stepOverEngine(engine));
   assert(stepOverEngine(engine));
   assert(not stepOverEngine(engine));
   assert(not stepOverEngine(engine));
   closeId(engine);
   closeId(mng);
   closeId(abstractWeights);
   closeId(distr);
end


function testGetCurrentTime()
   local delta = 0.0001;
   local distr = createDistribution(DISTR.EXP, 3.0);
   local abstractWeights = createAbstractWeights(3);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   local engine = createSimulationEngine();
   appendInterruptManager(engine, mng);
   local t0 = getCurrentTime(engine);
   assert(compareFloat(t0, 0.0, delta));
   stepOverEngine(engine);
   local t1 = getCurrentTime(engine);
   assert(t1 >= t0);
   stepOverEngine(engine);
   local t2 = getCurrentTime(engine);
   assert(t2 >= t1);
   stepOverEngine(engine);
   local t3 = getCurrentTime(engine);
   assert(t3 >= t2);
   closeId(engine);
   closeId(mng);
   closeId(abstractWeights);
   closeId(distr);
end


function testGetFutureTime()
   local delta = 0.0001;
   local distr = createDistribution(DISTR.EXP, 3.0);
   local abstractWeights = createAbstractWeights(3);
   local mng = createInterruptManager(abstractWeights, distr, nil);
   local engine = createSimulationEngine();
   appendInterruptManager(engine, mng);
   local t1 = getFutureTime(engine);
   stepOverEngine(engine);
   assert(compareFloat(t1, getCurrentTime(engine), delta));
   local t2 = getFutureTime(engine);
   stepOverEngine(engine);
   assert(compareFloat(t2, getCurrentTime(engine), delta));
   local t3 = getFutureTime(engine);
   stepOverEngine(engine);
   assert(compareFloat(t3, getCurrentTime(engine), delta));
   assert(compareFloat(getFutureTime(engine), -1.0, delta));
   closeId(engine);
   closeId(mng);
   closeId(abstractWeights);
   closeId(distr);
end


function testGetCurrentSource()
   local distr = createDistribution(DISTR.EXP, 3.0);

   local weights = createAbstractWeights(2);
   local weightsMng = createInterruptManager(weights, distr, nil);

   local resistors = createAnalogResistors(1);
   local resistorsMng = createInterruptManager(resistors, distr, nil);

   local engine = createSimulationEngine();
   appendInterruptManager(engine, weightsMng);
   appendInterruptManager(engine, resistorsMng);

   assert(getCurrentSource(engine) == 0);
   local weightsIntCount = 0;
   local resistorsIntCount = 0;

   stepOverEngine(engine);
   local s1 = getCurrentSource(engine);
   if s1 == weightsMng then
      weightsIntCount = weightsIntCount + 1;
   elseif s1 == resistorsMng then
      resistorsIntCount = resistorsIntCount + 1;
   end

   stepOverEngine(engine);
   local s1 = getCurrentSource(engine);
   if s1 == weightsMng then
      weightsIntCount = weightsIntCount + 1;
   elseif s1 == resistorsMng then
      resistorsIntCount = resistorsIntCount + 1;
   end

   stepOverEngine(engine);
   local s1 = getCurrentSource(engine);
   if s1 == weightsMng then
      weightsIntCount = weightsIntCount + 1;
   elseif s1 == resistorsMng then
      resistorsIntCount = resistorsIntCount + 1;
   end

   assert(weightsIntCount == 2);
   assert(resistorsIntCount == 1);

   closeId(engine);
   closeId(resistorsMng);
   closeId(resistors);
   closeId(weightsMng);
   closeId(weights);
   closeId(distr);
end


function testGetFutureSource()
   local distr = createDistribution(DISTR.EXP, 3.0);

   local weights = createAbstractWeights(2);
   local weightsMng = createInterruptManager(weights, distr, nil);

   local resistors = createAnalogResistors(1);
   local resistorsMng = createInterruptManager(resistors, distr, nil);

   local engine = createSimulationEngine();
   appendInterruptManager(engine, weightsMng);
   appendInterruptManager(engine, resistorsMng);

   local s1 = getFutureSource(engine);
   stepOverEngine(engine);
   assert(s1 == getCurrentSource(engine));
   local s2 = getFutureSource(engine);
   stepOverEngine(engine);
   assert(s2 == getCurrentSource(engine));
   local s3 = getFutureSource(engine);
   stepOverEngine(engine);
   assert(s3 == getCurrentSource(engine));
   assert(getFutureSource(engine) == 0);

   closeId(engine);
   closeId(resistorsMng);
   closeId(resistors);
   closeId(weightsMng);
   closeId(weights);
   closeId(distr);
end


-- Test common API;
testCloseId();

-- Test abstract neuron API;
testCreateAbstractConnectors();
testGetSignals();
testSetSignals();
testCreateAbstractWeights();
testGetAbstractWeights();
testSetAbstractWeights();
testCreateAbstractNeuron();
testComputeAbstractNeurons();
testComputeAbstractNeuronsC();
testTrainBPAbstractNeurons();

-- Test analog neuron API;
testCreateAnalogCapacitors();
testGetAnalogCapacitances();
testSetAnalogCapacitances();
testCreateAnalogComparators();
testCreateAnalogResistors();
testSetupAnalogResistors();
testGetAnalogResistances();
testSetAnalogResistances();
testCreateAnalogWires();
testGetPotentials();
testSetPotentials();
testCreateAnalogNeuron();
testComputeAnalogNeurons();
testComputeAnalogLimNeuronsC();

-- Test digital neuron API;
testCreateDigitalConnectors();
testGetValues();
testSetValues();
testCreateMemoryModule();
testGetDigitalWeights();
testSetDigitalWeights();
testCreateDigitalNeuron();
testComputeDigitalNeurons();
testTrainBPDigitalNeurons();

-- Test math API;
testCreateActFunc();
testCreateProcUnit();
testCreateDistribution();
testCalcMeanCI();
testCalcACProbabilityCI();

-- Test simulation engine API;
testCreateInterruptManager();
testCreateSimulationEngine();
testAppendInterruptManager();
testGetIntSourcesCount();
testGetInterruptsCount();
testSimulateInterrupt();
testRestartEngine();
testStepOverEngine();
testGetCurrentTime();
testGetFutureTime();
testGetCurrentSource();
testGetFutureSource();
